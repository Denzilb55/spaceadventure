﻿using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class CommanderView : MonoBehaviour
{

    public float mapWidth;
    public float mapHeight;

    public float width;
    public float height;

    public Material lineMaterial;
    public Color gridColour;

    public EntityManager entityManager;

    void Awake()
    {
        DrawGrid();
    }

    void DrawGrid()
    {
        int numHorizontal = 100;
        int numVertical = 100;

        int verticalArea = 5000;
        int horizontalArea = 5000;

        for (int i = 0; i < numVertical; ++i)
        {
            GameObject lineRendererGO = new GameObject();
            lineRendererGO.layer = gameObject.layer;
            lineRendererGO.transform.SetParent(transform);
            LineRenderer lineRenderer = lineRendererGO.AddComponent<LineRenderer>();
            lineRenderer.SetColors(gridColour, gridColour);
            lineRenderer.sharedMaterial = lineMaterial;
            lineRenderer.SetPositions(new Vector3[] { new Vector3(-verticalArea, i * verticalArea / numVertical - verticalArea / 2f, 0), new Vector3(verticalArea, i * verticalArea / numVertical - verticalArea / 2f, 0) });
        }

        for (int j = 0; j < numHorizontal; ++j)
        {
            GameObject lineRendererGO = new GameObject();
            lineRendererGO.layer = gameObject.layer;
            lineRendererGO.transform.SetParent(transform);
            LineRenderer lineRenderer = lineRendererGO.AddComponent<LineRenderer>();
            lineRenderer.SetColors(gridColour, gridColour);
            lineRenderer.sharedMaterial = lineMaterial;
            lineRenderer.SetPositions(new Vector3[] { new Vector3(j * horizontalArea / numHorizontal - horizontalArea / 2f, -horizontalArea, 0), new Vector3(j * horizontalArea / numHorizontal - horizontalArea / 2f, horizontalArea, 0) });
        }
    }
}
