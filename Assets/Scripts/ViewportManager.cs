﻿using UnityEngine;
using System.Collections.Generic;

public class ViewportManager : MonoBehaviour
{
  public enum ViewportLocations
  {
    Centre,
    BottomLeft,
    BottomRight,
    TopLeft,
    TopRight
  }

  public List<Viewport> viewportTemplates;

  public Dictionary<ViewportLocations, Viewport> openViewports;

  // Use this for initialization
  void Awake()
  {
    openViewports = new Dictionary<ViewportLocations, Viewport>();
  }

  // Update is called once per frame
  void Update()
  {

  }

  /// <summary>
  /// Close a viewport at the specified location if it exists
  /// </summary>
  /// <param name="location">The screen location of the viewport to close</param>
  public void CloseViewport(ViewportLocations location)
  {
    if (openViewports.ContainsKey(location))
    {
      Destroy(openViewports[location].gameObject);
      openViewports.Remove(location);
    }
    DisplayViewports();
  }

  /// <summary>
  /// Opens a viewport of specified type at specified location
  /// </summary>
  /// <param name="type">The type of the viewport</param>
  /// <param name="location">The preferred screen location of the viewport</param>
  /// <returns>The instantiated viewport instance</returns>
  public Viewport OpenViewport(Viewport.ViewportTypes type, ViewportLocations location)
  {
    Viewport viewport;
    if (openViewports.ContainsKey(location))
    {
      if (openViewports[location].GetViewportType() != type)
      {
        Destroy(openViewports[location].gameObject);
        openViewports.Remove(location);
        viewport = Instantiate<Viewport>(viewportTemplates[(int)type]);
        openViewports.Add(location, viewport);
      }
      else
      {
        viewport = openViewports[location];
      }
    }
    else
    {
      viewport = Instantiate<Viewport>(viewportTemplates[(int)type]);
      openViewports.Add(location, viewport);
    }
    
    viewport.preferredLocation = location;

    DisplayViewports();

    return viewport;
  }

  public Viewport GetViewport (ViewportLocations location)
  {
    return openViewports[location];
  }

  void DisplayViewports()
  {
    Viewport botLeftPort = null;
    Viewport botRightPort = null;
    Viewport bottomLeftPort = null;
    Viewport bottomRightPort = null;
    Viewport centrePort = null;
    
    openViewports.TryGetValue(ViewportLocations.Centre, out centrePort);
    openViewports.TryGetValue(ViewportLocations.BottomLeft, out botLeftPort);
    openViewports.TryGetValue(ViewportLocations.BottomRight, out botRightPort);
    openViewports.TryGetValue(ViewportLocations.TopLeft, out bottomLeftPort);
    openViewports.TryGetValue(ViewportLocations.TopRight, out bottomRightPort);
      
    if (centrePort)
    {
      centrePort.viewportCamera.rect = new Rect(0, 0, 1, 1);
    }
    if (botLeftPort)
    {      
      botLeftPort.viewportCamera.rect = new Rect(0.025f, 0.05f, 0.325f, 0.4f);
    }
    if (bottomLeftPort)
    {
      botLeftPort.viewportCamera.rect = new Rect(0.025f, 0.55f, 0.325f, 0.4f);
    }
    if (botRightPort)
    {
      botRightPort.viewportCamera.rect = new Rect(0.65f, 0.05f, 0.325f, 0.4f); 
    }
    if (bottomRightPort)
    {
      bottomRightPort.viewportCamera.rect = new Rect(0.65f, 0.55f, 0.325f, 0.4f);
    }
  }
}
