﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class TileNode
{
  public TileNode upNode;
  public TileNode downNode;
  public TileNode rightNode;
  public TileNode leftNode;

  // This is used for pathfinding
  public TileNode parentNode;

  public GameObject gameObject;
  public Vector2 gridPos;

  public TileNode(GameObject obj)
  {
    gameObject = obj;
    gridPos = new Vector2((int) obj.transform.position.x, (int)obj.transform.position.y);
  }

  public bool HasBreach()
  {
    foreach (Transform child in gameObject.transform)
    {
      if (child.CompareTag("HullBreach"))
      {
        return true;
      }
    }

    return false;
  }
}

