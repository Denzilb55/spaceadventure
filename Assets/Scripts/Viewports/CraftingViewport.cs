﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class CraftingViewport : Viewport
{
  public override ViewportTypes GetViewportType() 
  {
    return ViewportTypes.Crafting;
  }
}
