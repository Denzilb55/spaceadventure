﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class SpaceViewport : Viewport
{
  public override ViewportTypes GetViewportType()
  {
    return ViewportTypes.Space;
  }
}
