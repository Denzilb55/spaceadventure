﻿  using System;
  using System.Collections.Generic;

  using UnityEngine;

public abstract class Viewport : MonoBehaviour
{
  public enum ViewportTypes
  {
    Space,
    Commander,
    Crafting,
    Weapons
  }

  public Camera viewportCamera;

  public ViewportManager.ViewportLocations preferredLocation;

  /// <summary>
  ///  Sets the viewport to track an arbitrary transform
  /// </summary>
  /// <param name="focusObject">The object to focus</param>
  public void FocusTransform(Transform focusObject)
  {
    viewportCamera.transform.SetParent(focusObject);
    Vector3 prevPos = viewportCamera.transform.localPosition;
    viewportCamera.transform.localPosition = new Vector3(0, 0, prevPos.z);
    viewportCamera.transform.localEulerAngles = Vector3.zero;
  }

  void OnDestroy()
  {
    GameManager.instance.viewportManager.CloseViewport(preferredLocation);
  }

  public abstract ViewportTypes GetViewportType();
}