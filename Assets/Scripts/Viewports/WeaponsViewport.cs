﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class WeaponsViewport : Viewport
{
  public override ViewportTypes GetViewportType()
  {
    return ViewportTypes.Weapons;
  }
}
