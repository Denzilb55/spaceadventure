﻿using UnityEngine;
using System.Collections;

namespace Game.Room
{
  public class ControlRoom : Room
  {
    // Use this for initialization
    void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override bool Interact(PlayerCrewMan player)
    {
      //ship.Steer(cInput.GetAxis("VerticalMovement" + player.ControllerId), cInput.GetAxis("HorisontalMovement" + player.ControllerId));

      return base.Interact(player);
    }

    public override void StopInteract(PlayerCrewMan player)
    {
      ship.Steer(0, 0);
      base.StopInteract(player);
    }

    public override RoomType GetRoomType()
    {
      return RoomType.Control;
    }
  }
}
