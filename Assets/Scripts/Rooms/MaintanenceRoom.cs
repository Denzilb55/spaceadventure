﻿using UnityEngine;
using System.Collections;

namespace Game.Room
{
  public class MaintanenceRoom : Room
  {
    public GameObject crate;

    private bool _hasStock;

    public bool hasStock
    {
      get { return _hasStock; }
      set {
        _hasStock = value;

    //    crate.SetActive(_hasStock);
      }
    }

    // Use this for initialization
    void Awake()
    {
      hasStock = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    int repairProgress = 0;

    public override bool Interact(PlayerCrewMan player)
    {
      if (!hasStock)
      {
        return false;
      }

      if (cInput.GetKeyDown("Primary" + player.ControllerId))
      {
        repairProgress++;

        if (repairProgress == 10)
        {
          ship.Repair(10);
          hasStock = false;
          repairProgress = 0;
          return false;
        }
      }
      return true;
    }

    public override void StopInteract(PlayerCrewMan player)
    {
      base.StopInteract(player);
    }

    public override RoomType GetRoomType()
    {
      return RoomType.Maintanence;
    }
  }
}
