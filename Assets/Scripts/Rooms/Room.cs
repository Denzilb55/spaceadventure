﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Game.Room
{
  public enum RoomType
  {
    Passage,
    Control,
    Weapons,
    Shield,
    Maintanence,
    Engine,
    Cargo
  }

  public abstract class Room : MonoBehaviour
  {
    // The players in the room
    private List<GameObject> players = new List<GameObject>();

    public Viewport.ViewportTypes viewportType;

    protected Ship ship;

    public void SetOwner(Ship ship)
    {
      this.ship = ship;
    }

    public void EnterRoom(PlayerCrewMan player)
    {
      players.Add(player.gameObject);
      player.OnRoomEnter(this);
      Debug.Log(player.name + " entered room: " + name + " of " + ship.name);
    }

    public void ExitRoom(PlayerCrewMan player)
    {
      players.Remove(player.gameObject);
      player.OnRoomExit(this);
      Debug.Log(player.name + " exited room: " + name + " of " + ship.name);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
      if (other.CompareTag("Player"))
      {
        PlayerCrewMan player = other.GetComponent<PlayerCrewMan>();
        EnterRoom(player);
      }
    }

    void OnTriggerExit2D(Collider2D other)
    {
      if (other.CompareTag("Player"))
      {
        PlayerCrewMan player = other.GetComponent<PlayerCrewMan>();
        ExitRoom(player);
      }
    }

    /// <summary>
    /// Interacts with room.
    /// </summary>
    /// <param name="player"></param>
    /// <returns>true if continuous action, false otherwise</returns>
    public virtual bool Interact(PlayerCrewMan player)
    {
      Viewport commanderPort = GameManager.instance.viewportManager.OpenViewport(viewportType, GameManager.instance.playerViewportLocations[player.ControllerId]);
      commanderPort.FocusTransform(ship.transform);
      return true;
    }

    public virtual void StopInteract(PlayerCrewMan player)
    {
      if (player.IsInOwnShip)
      {
        GameManager.instance.viewportManager.CloseViewport(GameManager.instance.playerViewportLocations[player.ControllerId]);
      }
      else
      {
        Viewport spacePort = GameManager.instance.viewportManager.OpenViewport(Viewport.ViewportTypes.Space, GameManager.instance.playerViewportLocations[player.ControllerId]);
        spacePort.FocusTransform(player.transform);
      }
    }

    public abstract RoomType GetRoomType();
  }
}
