﻿using UnityEngine;
using System.Collections;

namespace Game.Room
{
  public class ShieldRoom : Room
  {
    public Shield shield;

    // Use this for initialization
    void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void CoolDown()
    {

    }

    public override bool Interact(PlayerCrewMan player)
    {
      if (!IsInvoking("CoolDown"))
      {
        if (cInput.GetKeyDown("Primary" + player.ControllerId))
        {
          Invoke("CoolDown", 0.3f);
          shield.Restore(5);
        }
      }
      return base.Interact(player);
    }

    public override void StopInteract(PlayerCrewMan player)
    {
      base.StopInteract(player);
    }

    public override RoomType GetRoomType()
    {
      return RoomType.Shield;
    }
  }
}
