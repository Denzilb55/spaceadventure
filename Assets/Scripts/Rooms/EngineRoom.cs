﻿using UnityEngine;
using System.Collections;

namespace Game.Room
{
  public class EngineRoom : Room
  {
    // Use this for initialization
    void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override bool Interact(PlayerCrewMan player)
    {
      return base.Interact(player);
    }

    public override void StopInteract(PlayerCrewMan player)
    {
      base.StopInteract(player);
    }

    public override RoomType GetRoomType()
    {
      return RoomType.Engine;
    }
  }
}
