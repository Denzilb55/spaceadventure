﻿using UnityEngine;
using System.Collections.Generic;

namespace Game.Room
{
  public class CargoRoom : Room
  {
    public List<GameObject> cargoCrates;

    public int _cargoCount = 0;
    private MaintanenceRoom maintanenceRoom;

    public int cargoCount
    {
      set
      {
        _cargoCount = value;

        int showCrates = (int)Mathf.Ceil(_cargoCount / 10.0f);

        showCrates = Mathf.Clamp(showCrates, 0, cargoCrates.Count);

        for (int i = 0; i < showCrates; i++)
        {
          cargoCrates[i].SetActive(true);
        }

        for (int i = showCrates; i < cargoCrates.Count; i++)
        {
          cargoCrates[i].SetActive(false);
        }
      }

      get
      {
        return _cargoCount;
      }
    }

    // Use this for initialization
    void Awake()
    {
      cargoCount = 5;
      maintanenceRoom = transform.parent.GetComponentInChildren<MaintanenceRoom>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override bool Interact(PlayerCrewMan player)
    {
      if (cInput.GetKeyDown("Primary" + player.ControllerId))
      {
        if (!maintanenceRoom.hasStock && cargoCount > 0)
        {
          maintanenceRoom.hasStock = true;
          cargoCount--;
        }
      }

      return false;
    }

    public override void StopInteract(PlayerCrewMan player)
    {
      base.StopInteract(player);
    }

    public override RoomType GetRoomType()
    {
      return RoomType.Cargo;
    }
  }
}
