﻿using UnityEngine;
using System.Collections.Generic;

namespace Game.Room
{
  public class WeaponsRoom : Room
  {
    public List<Weapon> weapons;

    // Use this for initialization
    void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    int targetIndex = 0;
    
    private void CycleTargetLeft(int count)
    {
      targetIndex--;

      if (targetIndex < 0)
      {
        targetIndex = count - 1;
      }
    }

    private void CycleTargetRight(int count)
    {
      targetIndex++;

      if (targetIndex >= count)
      {
        targetIndex = 0;
      }
    }

    public override bool Interact(PlayerCrewMan player)
    {
      List<Ship> targets = EntityManager.instance.otherShips;
      if (cInput.GetKeyDown("CycleLeft1"))
      {
        CycleTargetLeft(targets.Count);
      }

      if (cInput.GetKeyDown("CycleRight1"))
      {
        CycleTargetRight(targets.Count);
      }

      Ship targetShip = targets[targetIndex];

      Viewport commanderPort = GameManager.instance.viewportManager.OpenViewport(Viewport.ViewportTypes.Space, GameManager.instance.playerViewportLocations[player.ControllerId]);
      commanderPort.FocusTransform(targets[targetIndex].transform);

      foreach (var weapon in weapons)
      {
        weapon.Aim(cInput.GetAxis("HorisontalMovement" + player.ControllerId), cInput.GetAxis("VerticalMovement" + player.ControllerId));

        if (cInput.GetKey("Primary" + player.ControllerId))
        {
          //weapon.Fire(targets[targetIndex].gameObject);
        }
      }


      return true;// base.Interact(player);
    }

    public override void StopInteract(PlayerCrewMan player)
    {
      base.StopInteract(player);
    }

    public override RoomType GetRoomType()
    {
      return RoomType.Shield;
    }
  }
}
