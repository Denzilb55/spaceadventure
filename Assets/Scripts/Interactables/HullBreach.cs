﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game.Entities.Task;
using UnityEngine;
using Game.Ships;

public class HullBreach : Interactable
{
  public float repairProgress
  {
    get;
    private set;
  }

  public override InteractableType GetInteractableType()
  {
    return InteractableType.HullBreach;
  }

  private void FixBreach(BaseCrewMan crewMan)
  {
    repairProgress += Time.deltaTime * 50;

    if (repairProgress >= 100)
    {
      Debug.Log(crewMan.name + " FIXED breach");
      Destroy(gameObject);
    }
  }

  public override void BeginUse(AICrewMan crew)
  {
    base.BeginUse(crew);
  }

  public override void EndUse(AICrewMan crew)
  {
    base.EndUse(crew);
  }

  public override void Interact(AICrewMan crew)
  {
    FixBreach(crew);
  }

  public override void Interact(PlayerCrewMan crew)
  {
    FixBreach(crew);
  }

  public override List<JobType> GetJobs()
  {
    List<JobType> jobTypeList = new List<JobType>();
    jobTypeList.Add(JobType.Repair);
    return jobTypeList;
  }

  public override int GetJobPriority(JobType jobType)
  {
    return 50;
  }
}
