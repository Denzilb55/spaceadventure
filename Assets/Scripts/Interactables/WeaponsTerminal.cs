﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game.Entities.Task;

public class WeaponsTerminal : Interactable
{
  public List<Weapon> linkedWeapons;

  Vector2 targetOffset;
  Ship targetShip;

  private void Update()
  {

  }

  public override bool IsAvailableToAI()
  {
    return base.IsAvailableToAI() && AllWeaponsReady();
  }

  public override void Interact(PlayerCrewMan crew)
  {
    if (cInput.GetKeyDown("Primary" + crew.ControllerId) && AllWeaponsReady())
    {
      StartCoroutine (SystematicFire());
    }

    targetOffset += new Vector2(cInput.GetAxis("HorisontalMovement" + crew.ControllerId), cInput.GetAxis("VerticalMovement" + crew.ControllerId)) * 0.1f;
    Viewport viewport = GameManager.instance.viewportManager.GetViewport(GameManager.instance.playerViewportLocations[crew.ControllerId]);
 
    viewport.viewportCamera.transform.localPosition = new Vector3(targetOffset.x, targetOffset.y, viewport.viewportCamera.transform.localPosition.z);

    if (cInput.GetKey("ZoomIn" + crew.ControllerId))
    {
      viewport.viewportCamera.orthographicSize -= Time.deltaTime * 3; 
    }
    else if (cInput.GetKey("ZoomOut" + crew.ControllerId))
    {
      viewport.viewportCamera.orthographicSize += Time.deltaTime * 3;
    }
  }

  IEnumerator SystematicFire()
  {
    if (targetShip != null)
    {
      foreach (Weapon weapon in linkedWeapons)
      {
        weapon.Fire(targetShip.gameObject, targetOffset);
        yield return new WaitForSeconds(0.3f);
      }
    }
  }

  public bool AllWeaponsReady()
  {
    foreach (Weapon weapon in linkedWeapons)
    {
      if (weapon.isCooldown)
      {
        return false;
      }
    }

    return true;
  }

  public override void BeginUse(PlayerCrewMan crew)
  {
    base.BeginUse(crew);
    targetOffset = Vector2.zero;
    List<Ship> targets = EntityManager.instance.allShips;

    foreach (var target in targets) {
      if (crew.ship != target)
      {
        targetShip = target;
        break;
      }
    }

    Viewport commanderPort = GameManager.instance.viewportManager.OpenViewport(Viewport.ViewportTypes.Weapons, GameManager.instance.playerViewportLocations[crew.ControllerId]);
    commanderPort.FocusTransform(targetShip.transform);
  }

  public override void EndUse(PlayerCrewMan crew)
  {
    base.EndUse(crew);
    GameManager.instance.viewportManager.CloseViewport(GameManager.instance.playerViewportLocations[crew.ControllerId]);
  }

  public override InteractableType GetInteractableType()
  {
    return InteractableType.WeaponsTerminal;
  }

  public override void Interact(AICrewMan crew)
  {
    StartCoroutine(SystematicFire());
  }

  public override void BeginUse(AICrewMan crew)
  {
    base.BeginUse(crew);
    targetOffset = Vector2.zero;
    List<Ship> targets = EntityManager.instance.allShips;

    foreach (var target in targets)
    {
      if (crew.ship != target)
      {
        targetShip = target;
        break;
      }
    }

    Debug.Log(crew.name + " using weapons terminal");
  }

  public override void EndUse(AICrewMan crew)
  {
    base.EndUse(crew);
  }

  // TODO: Only init list once
  public override List<JobType> GetJobs()
  {
    List<JobType> jobTypeList = new List<JobType>();
    jobTypeList.Add(JobType.Attack);
    return jobTypeList;
  }

  public override int GetJobPriority(JobType jobType)
  {
    return 5;
  }

}
