﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game.Entities.Task;
using System;

public class ResouceDispenser : Interactable
{
  public int initialResourceCount;
  int resourceCount;
  public Item itemTemplate;
  public string resourceName;

  public void Awake()
  {
    // An arbitraty initial value. Will be controlled by a resource
    // manager later on.
    resourceCount = 100;
  }

  public override void Interact(PlayerCrewMan crew)
  {
    if (cInput.GetKeyDown("Primary" + crew.ControllerId)) {
      if (resourceCount > 0) {
        // Simple placeholder code where the resources in this dispenser will 
        // be managed.
        resourceCount--;
        print(resourceName + " stock left: " + resourceCount);
        Item resourceInstance = Instantiate<Item>(itemTemplate);
        crew.PickUpItem(resourceInstance);
      }
    }
  }

  public override void BeginUse(PlayerCrewMan crew)
  {
    base.BeginUse(crew);
    // Do interaction
    Interact(crew);
    // And stop the interaction, since the player will only be picking up
    // or dropping off an object.
    crew.TryStopInteract();
  }

  public override void EndUse(PlayerCrewMan crew)
  {
    base.EndUse(crew);
  }

  public override InteractableType GetInteractableType()
  {
    return InteractableType.ResourcesDispenser;
  }

  public override void Interact(AICrewMan crew)
  {
    if (resourceCount > 0)
    {
      // Simple placeholder code where the resources in this dispenser will 
      // be managed.
      resourceCount--;
      print(resourceName + " stock left: " + resourceCount);
      Item resourceInstance = Instantiate<Item>(itemTemplate);
      crew.PickUpItem(resourceInstance);
    }
  }

  public override void BeginUse(AICrewMan crew)
  {
    base.BeginUse(crew);
    // Do interaction
    Interact(crew);
    // And stop the interaction, since the player will only be picking up
    // or dropping off an object.
    crew.TryStopInteract();
  }

  public override void EndUse(AICrewMan crew)
  {
    base.EndUse(crew);
  }

  public override List<JobType> GetJobs()
  {
    return new List<JobType>();
  }

  public override int GetJobPriority(JobType jobType)
  {
    return 0;
  }
}
