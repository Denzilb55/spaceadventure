﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game.Entities.Task;

public class ShieldTerminal : Interactable, IInventory
{
  public Shield shield;
  private float shieldEnergy;

  public Item cellSlot;

  private void Update()
  {
    shieldEnergy += Time.deltaTime * 3;
  }

  public bool DoesJobRequireItem(JobType job)
  {
    if (job == JobType.Shield)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  public bool IsItemRequired(ItemType itemType, JobType job)
  {
    if (job == JobType.Shield)
    {
      return (itemType == ItemType.AdvancedEnergyCell || itemType == ItemType.EnergyCell);
    }
    else
    {
      return false;
    }
  }

  public bool IsItemRequired(ItemType itemType)
  {
    foreach (JobType job in GetJobs())
    {
      if (IsItemRequired(itemType, job))
      {
        return true;
      }
    }

    return false;
  }

  public bool HasRequiredItem(JobType job)
  {
    if (DoesJobRequireItem(job))
    {
      if (cellSlot != null)
      {
        return IsItemRequired(cellSlot.itemType, job);
      }
      return false;
    }
    else
    {
      return true;
    }
  }

  private void RestoreShield()
  {
    if (HasRequiredItem(JobType.Shield))
    {
      shield.Restore(100);
      Destroy(cellSlot.gameObject);
      cellSlot = null;
    }

  }

  public override void Interact(PlayerCrewMan crew)
  {
    
  }

  public override void BeginUse(PlayerCrewMan crew)
  {
    base.BeginUse(crew);

    if (crew.GetItem() != null)
    {
      print("Adding item to the list");
      crew.DropItem(this);
      crew.TryStopInteract();
    }
    else
    {
      RestoreShield();
    }
  }

  public override void EndUse(PlayerCrewMan crew)
  {
    base.EndUse(crew);
  }

  public override InteractableType GetInteractableType()
  {
    return InteractableType.ShieldTerminal;
  }


  public override void Interact(AICrewMan crew)
  {
    RestoreShield();
  }

  public override void BeginUse(AICrewMan crew)
  {
    base.BeginUse(crew);
    Debug.Log(crew.name + " using shield terminal");

    if (crew.GetItem() != null)
    {
      print("Adding item to the list");
      crew.DropItem(this);
      crew.TryStopInteract();
    }
    else
    {
      RestoreShield();
    }
  }

  public override void EndUse(AICrewMan crew)
  {
    base.EndUse(crew);
  }

  // TODO: Only init list once
  public override List<JobType> GetJobs()
  {
    List<JobType> jobTypeList = new List<JobType>();
    jobTypeList.Add(JobType.Shield);
    if (!HasRequiredItem(JobType.Shield))
    {
      /* if (ship.itemRegistry.FindItem(ItemType.EnergyCell) || ship.itemRegistry.FindItem(ItemType.AdvancedEnergyCell))
       {
         jobTypeList.Add(JobType.LoadShieldWithCell);
       }
       else
       {
         jobTypeList.Add(JobType.CraftEnergyCell);
       }*/

      jobTypeList.Add(JobType.LoadShieldWithCell);
    }
    return jobTypeList;
  }

  public override int GetJobPriority(JobType jobType)
  {
    switch (jobType)
    {
      case JobType.Shield:
        {
          if (shield.isActiveAndEnabled)
          {
            return 0;
          }
          return 10;
        }

      case JobType.LoadShieldWithCell:
        {
          if (shield.isActiveAndEnabled)
          {
            return 0;
          }
          return 9;
        }

      case JobType.CraftEnergyCell:
        {
          if (shield.isActiveAndEnabled)
          {
            return 1;
          }
          return 6;
        }
    }
    return 0;
  }

  public override bool IsAvailableToAI()
  {
  //  Debug.Log(this + " HAS REQUIRED ITEM: " + HasRequiredItem(JobType.Shield));
    return base.IsAvailableToAI() && HasRequiredItem(JobType.Shield);
  }

  public override UnavailableCause GetUnavailableCauses()
  {
    UnavailableCause cause = base.GetUnavailableCauses();

    cause = HasRequiredItem(JobType.Shield) ? cause | UnavailableCause.RequireItem : cause;

    return cause;
  }

  public bool AddItemToInventory(Item item)
  {
    Debug.Log("DROP ITEM IN SHIELD");
    cellSlot = item;
    return true;
  }

  public Item FindAvailableItem(ItemType itemType)
  {
    throw new NotImplementedException();
  }

  public Item FindItem(ItemType itemType)
  {
    throw new NotImplementedException();
  }

  public bool RemoveItem(Item item)
  {
    throw new NotImplementedException();
  }
}
