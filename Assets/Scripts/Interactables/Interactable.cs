﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Game.Entities.Task;
using Game.Ships;

public enum InteractableType
{
  WeaponsTerminal,
  ShieldTerminal,
  CraftingTerminal,
  ResourcesDispenser,
  HullBreach,

  Item
}

[Flags]
public enum UnavailableCause
{
  None,

  NotRecharged = 0x001,
  RequireItem  = 0x010,
  InUse        = 0x100
}

public abstract class Interactable : MonoBehaviour, IJobLister
{
  protected TileNode tileNode;
  protected Ship ship;
  public BaseCrewMan interactingCrewMan;

  // Is the object being interacted with?
  public bool isOccupied
  {
    get
    {
      return interactingCrewMan != null;
    }
  }

  // Is the object flagged by an AI crewman in order
  // to prevent other AI from approaching it to do a task.
  public bool isAIFlagged;

  // This is used for AI to communicate to each other
  // that they are on the way to the object and another
  // AI should not try to interact with this.
  protected AICrewMan allocatedAICrewMan; 

  public virtual void Initialise(Ship ship, TileNode node)
  {
    this.ship = ship;
    this.tileNode = node;
    RegisterWithShipAI();
  }

  private void OnDestroy()
  {
    print(gameObject + " has been destroyed");
    if (ship != null)
    {
      ship.NotifyInteractableDestroy(this);
    }
  }

  public TileNode GetTileNode()
  {
    return tileNode;
  }

  public Vector2 gridPos
  {
    get
    {
      return new Vector2((int)(transform.position.x + 0.5f), (int)(transform.position.y + 0.5f));
    }
  }

  // Flag for use by AI
  public void Flag(AICrewMan aiCrew)
  {
    Debug.Log(this + " flagged for use by " + aiCrew.name);
    isAIFlagged = true;
    allocatedAICrewMan = aiCrew;
  }

  public void Unflag(AICrewMan aiCrew)
  {
    Debug.Log(this + " unflagged for use by " + aiCrew.name);

    // Debug, if this assert triggers, it means an AI is unflagging an interatable
    // used by another AI. It would be possible to just exit if this is happening,
    // but the task system shouldn't be in a state where mutliple AI try to use
    // the same interactable.
    //
    // The reason may be due to a bug in the task system handling, or due to
    // individual tasks/ interatables assigning an interactable that is not
    // actually available.
    //
    // This may actually be OK for instance, if two AI are dropping things in
    // the crafting table. However, they probably should not be flagging the
    // interatable in that case because their action is not blocking to each other.
    Debug.Assert(allocatedAICrewMan == aiCrew || allocatedAICrewMan == null);
    isAIFlagged = false;
    allocatedAICrewMan = null;    
  }

  public bool IsOccupiedByOtherThan(BaseCrewMan crewMan)
  {
    return crewMan != interactingCrewMan && interactingCrewMan != null;
  }

  public bool IsOccupiedBy(BaseCrewMan crewMan)
  {
    return crewMan == interactingCrewMan;
  }

  public void RegisterWithShipAI()
  {
    ship.shipAI.RegisterJobLister(this);
  }

  public virtual bool IsAvailableToAI()
  {
   // Debug.Log(this + " is flagged: " + isAIFlagged + " and is occupied: " + isOccupied + " by " + interactingCrewMan);


    return !isOccupied && !isAIFlagged;
  }

  public virtual UnavailableCause GetUnavailableCauses()
  {
    UnavailableCause cause = UnavailableCause.None;

    cause = isOccupied ? cause | UnavailableCause.InUse : cause;
    cause = isAIFlagged ? cause | UnavailableCause.InUse : cause;

    return cause;
  }

  public bool IsUnavailableDueTo(UnavailableCause cause)
  {
    return (cause & GetUnavailableCauses()) != 0;
  }

  public virtual void BeginUse(AICrewMan crew)
  {
    Debug.Log(crew.name + " begins using " + this);
    interactingCrewMan = crew;
  }

  public virtual void BeginUse(PlayerCrewMan crew)
  {
    interactingCrewMan = crew;
    // CONSIDER KICKING OFF AI HERE
    //isAIFlagged = false;
    //allocatedAICrewMan = null;
  }

  public virtual void EndUse(AICrewMan crew)
  {
    // TODO: this is called multiple times sometimes due to tasks
    // being ended by task and by AI. Fix
    if (interactingCrewMan == null)
    {
      return;
    }
    print(crew + " ends use of " + this);
    Unflag(crew);
    interactingCrewMan = null;
    crew.StopInteract();
  }

  public virtual void EndUse(PlayerCrewMan crew)
  {
    interactingCrewMan = null;
    isAIFlagged = false;
    crew.StopInteract();
  }

  public abstract InteractableType GetInteractableType();
  public abstract void Interact(AICrewMan crew);
  public abstract void Interact(PlayerCrewMan crew);

  public abstract List<JobType> GetJobs();

  public abstract int GetJobPriority(JobType jobType);

}
