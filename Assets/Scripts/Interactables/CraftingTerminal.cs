﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game.Entities.Task;

enum Area
{
  eCrafting,
  eInventory,
  eRecipe,
  AREA_COUNT
}

public class CraftingTerminal : Interactable, IInventory
{
  CraftingViewport craftingPort;

  public GameObject[] floorTiles;
  public GameObject inventoryTile;
  public GameObject selectorTile;
  public float moveTime;
  float inverseMoveTime;

  Grid inventoryGrid;
  Grid craftGrid;
  Grid recipeGrid;

  GridPosition selectorGridPosition;
  Area currentArea;

  Vector2 tileSize;
  Dictionary<Area, Vector2> areaPositions;
  Dictionary<Area, Grid> areaGrids;

  GameObject selector;
  Vector3 targetPosition;

  bool moving = false;

  List<Recipe> knownRecipes;

  private bool pickUpItem;
  private bool dropItem;

  public bool ContainsIngredientsToCraft(ItemType itemType)
  {
    foreach (Recipe recipe in knownRecipes)
    {
      if (recipe.Type == itemType)
      {
        List<ItemType> requiredItems = new List<ItemType>();
        foreach (ItemType requiredItem in recipe.ItemGridPositions)
        {
          if (requiredItem != ItemType.None)
          {
            requiredItems.Add(requiredItem);
          }
        }

        foreach (GridPosition pos in craftGrid.Cells())
        {
          Item cellItem = craftGrid.Cell(pos);

          if (cellItem != null)
          {
            if (requiredItems.Remove(cellItem.itemType))
            {
              if (requiredItems.Count == 0)
              {
                return true;
              }
            }
          }
        }

        foreach (GridPosition pos in inventoryGrid.Cells())
        {
          Item cellItem = inventoryGrid.Cell(pos);

          if (cellItem != null)
          {
            if (requiredItems.Remove(cellItem.itemType))
            {
              if (requiredItems.Count == 0)
              {
                return true;
              }
            }
          }
        }

        if (requiredItems.Count == 0)
        {
          return true;
        }
      }
    }
    
    return false;
  }

  // TODO: Remove temp method. AI needs to fill in spots too (so that there is progression if they are interrupted)
  public Item CraftItemFromIngredients(ItemType itemType, AICrewMan crewman)
  {
    foreach (Recipe recipe in knownRecipes)
    {
      if (recipe.Type == itemType)
      {
        List<ItemType> requiredItems = new List<ItemType>();
        foreach (ItemType requiredItem in recipe.ItemGridPositions)
        {
          if (requiredItem != ItemType.None)
          {
            requiredItems.Add(requiredItem);
          }
        }

        // items used for this crafting
        List<Item> usedItems = new List<Item>();

        foreach (GridPosition pos in craftGrid.Cells())
        {
          Item cellItem = craftGrid.Cell(pos);

          if (cellItem != null)
          {
            if (requiredItems.Remove(cellItem.itemType))
            {
              usedItems.Add(craftGrid.PopFromCell(pos));
            }
          }
        }

        foreach (GridPosition pos in inventoryGrid.Cells())
        {
          Item cellItem = inventoryGrid.Cell(pos);

          if (cellItem != null)
          {
            if (requiredItems.Remove(cellItem.itemType))
            {
              usedItems.Add(inventoryGrid.PopFromCell(pos));
            }
          }
        }

        if (requiredItems.Count == 0)
        {
          Debug.Log("CRAFTING " + recipe.Type);
          foreach (Item usedItem in usedItems)
          {
            Destroy(usedItem.gameObject);
          }
          return Instantiate<Item>(ItemManager.instance.items[(int)recipe.Type], crewman.transform);
        }
      }
    }

    Debug.Assert(false, "Should not be here. You need to check that you have the correct items before trying to craft!");
    return null;
  }

  public override void Initialise(Ship ship, TileNode node)
  {
    base.Initialise(ship, node);
    this.ship = ship;
    this.tileNode = node;

    Vector2 terminalPosition = Vector2.zero;
    int terminalId = EntityManager.instance.RegisterTerminal(ref terminalPosition);
    if (terminalId == -1)
    {
      gameObject.SetActive(false);
    }

    inventoryGrid = new Grid(2, 3);
    craftGrid = new Grid(3, 3);
    recipeGrid = new Grid(6, 1);

    areaGrids = new Dictionary<Area, Grid>();
    areaGrids.Add(Area.eCrafting, craftGrid);
    areaGrids.Add(Area.eInventory, inventoryGrid);
    areaGrids.Add(Area.eRecipe, recipeGrid);

    // Sizes of the grids used for the inventory and craft areas
    Size inventorySize = inventoryGrid.Size();
    Size craftSize = craftGrid.Size();
    // Variables used for selector control
    selectorGridPosition = new GridPosition(0, 0);
    currentArea = Area.eCrafting;

    // Size of each tile in the inventory and craft grid
    tileSize = new Vector2(1, 1);
    // The top-left positions of the grids.
    areaPositions = new Dictionary<Area, Vector2>();
    areaPositions.Add(Area.eInventory, new Vector2(-2, 0.25f) + terminalPosition);
    areaPositions.Add(Area.eCrafting, new Vector2(0.5f, 0.25f) + terminalPosition);
    areaPositions.Add(Area.eRecipe, new Vector2(-2.25f, 1.75f) + terminalPosition);

    inverseMoveTime = 1 / moveTime;

    foreach (GridPosition position in inventoryGrid.Cells())
    {
      Vector2 inventoryPosition = areaPositions[Area.eInventory];
      Instantiate<GameObject>(inventoryTile, new Vector3(inventoryPosition.x + position.x * tileSize.x, inventoryPosition.y - position.y * tileSize.y, 0), new Quaternion());
    }

    foreach (GridPosition position in craftGrid.Cells())
    {
      Vector2 craftPosition = areaPositions[Area.eCrafting];
      int index = position.x + position.y * craftSize.width;
      GameObject toInstantiate = floorTiles[index < floorTiles.Length ? index : 0];
      Instantiate<GameObject>(toInstantiate, new Vector3(craftPosition.x + position.x * tileSize.x, craftPosition.y - position.y * tileSize.y, 0), new Quaternion());
    }

    foreach (GridPosition position in recipeGrid.Cells())
    {
      Vector2 recipePosition = areaPositions[Area.eRecipe];
      Instantiate<GameObject>(inventoryTile, new Vector3(recipePosition.x + position.x * tileSize.x, recipePosition.y - position.y * tileSize.y, 0), new Quaternion());
    }

    Vector2 currentPosition = areaPositions[currentArea];
    targetPosition = new Vector3(currentPosition.x, currentPosition.y, -0.1f);
    selector = Instantiate<GameObject>(selectorTile, targetPosition, new Quaternion());

    knownRecipes = new List<Recipe>();
    List<ItemType> addRecipes = new List<ItemType>();
    addRecipes.Add(ItemType.EnergyCell);
    addRecipes.Add(ItemType.AdvancedEnergyCell);

    foreach (ItemType type in addRecipes)
    {
      RecipeBuilder rb;
      switch (type)
      {
        default:
        case ItemType.EnergyCell:
          rb = new PowerCellBuilder();
          break;
        case ItemType.AdvancedEnergyCell:
          rb = new AdvancedPowerCellBuilder();
          break;
      }
      knownRecipes.Add(rb.getRecipe());
    }

    foreach (Recipe recipe in knownRecipes)
    {
      GridPosition? openPosition = recipeGrid.GetFirstOpenCell();

      if (openPosition.HasValue)
      {
        Vector2 recipePosition = areaPositions[Area.eRecipe];
        Item instance = Instantiate<Item>(ItemManager.instance.items[(int)recipe.Type],
          new Vector3(recipePosition.x + openPosition.Value.x * tileSize.x, recipePosition.y - openPosition.Value.y * tileSize.y, -0.1f), new Quaternion(), this.transform);
        recipeGrid.DropOnCell(openPosition.Value, instance);
        instance.transform.localScale = Vector3.one;
      }
      else
      {
        break;
      }
    }
  }

  private bool CheckRecipe(ItemType[,] recipe) {
    for (int i = 0; i <= 2; i++) {
      for (int j = 0; j <= 2; j++) {
        if (craftGrid.CellItemTag(i, j) != recipe[j, i]) {
          return false;
        }
      }
    }
    return true;
  }

  private void DestroyRecipeItems(ItemType[,] recipe)
  {
    for (int i = 0; i <= 2; i++)
    {
      for (int j = 0; j <= 2; j++)
      {
        ItemType itemType = craftGrid.CellItemTag(i, j);
        if (itemType != ItemType.None && itemType == recipe[j, i])
        {
           Destroy(craftGrid.PopFromCell(new GridPosition(i, j)).gameObject);
        }
      }
    }
  }

  private Item TryCraftingItem(BaseCrewMan crewman) {
    foreach (Recipe recipe in knownRecipes) {
      if (CheckRecipe(recipe.ItemGridPositions)) {
        DestroyRecipeItems(recipe.ItemGridPositions);
        return Instantiate<Item>(ItemManager.instance.items[(int)recipe.Type], crewman.transform);
      }
    }
    return null;
  }

  public override void Interact(PlayerCrewMan crewman)
  {    
    if (cInput.GetKeyDown("KBPrimary" + crewman.ControllerId) || cInput.GetKeyDown("Primary" + crewman.ControllerId)) {
      if (selector.transform.childCount == 0) {
        QueuePickUpItem();
      }
      else {
        QueueDropItem();
      }
    }
    if (cInput.GetKeyDown("KBSecondary" + crewman.ControllerId) || cInput.GetKeyDown("Y" + crewman.ControllerId)) {
      // Craft an item
      if (selector.transform.childCount == 0)
      {
        Item newlyCrafteItem = TryCraftingItem(crewman);
        if (newlyCrafteItem != null)
        {
          crewman.PickUpItem(newlyCrafteItem);
          crewman.TryStopInteract();
        }
      }
      else
      {
        Item item = selector.transform.GetChild(0).GetComponent<Item>();
        crewman.PickUpItem(item);
        crewman.TryStopInteract();
      }

    }

    if (!moving) {
      if (cInput.GetKey("KBLeft" + crewman.ControllerId) || cInput.GetAxis("HorisontalMovement" + crewman.ControllerId) < -0.5f) {
        selectorGridPosition.x--;
        if (selectorGridPosition.x < 0) {
          if (currentArea == Area.eCrafting) {
            currentArea = Area.eInventory;
            selectorGridPosition.x = areaGrids[Area.eInventory].Size().width - 1;
          }
          else {
            selectorGridPosition.x++;
          }
        }
      }
      else if (cInput.GetKey("KBRight" + crewman.ControllerId) || cInput.GetAxis("HorisontalMovement" + crewman.ControllerId) > 0.5f) {
        selectorGridPosition.x++;
        if (selectorGridPosition.x >= areaGrids[currentArea].Size().width) {
          if (currentArea == Area.eInventory) {
            currentArea = Area.eCrafting;
            selectorGridPosition.x = 0;
          }
          else {
            selectorGridPosition.x--;
          }
        }
      }
      else if (cInput.GetKey("KBUp" + crewman.ControllerId) || cInput.GetAxis("VerticalMovement" + crewman.ControllerId) > 0.5f) {
        selectorGridPosition.y--;
        if (selectorGridPosition.y < 0) {
          if (currentArea == Area.eInventory || currentArea == Area.eCrafting) {
            selectorGridPosition.y = areaGrids[Area.eRecipe].Size().height - 1;
            int newX = selectorGridPosition.x;
            if (currentArea == Area.eCrafting) {
              newX = selectorGridPosition.x + areaGrids[Area.eRecipe].Size().width / 2;
            }
            selectorGridPosition.x = Mathf.Max(0, Mathf.Min(areaGrids[Area.eRecipe].Size().width - 1, newX));
            currentArea = Area.eRecipe;
          }
          else {
            selectorGridPosition.y++;
          }
        }

      }
      else if (cInput.GetKey("KBDown" + crewman.ControllerId) || cInput.GetAxis("VerticalMovement" + crewman.ControllerId) < -0.5f) {
        selectorGridPosition.y++;
        if (selectorGridPosition.y >= areaGrids[currentArea].Size().height) {
          if (currentArea == Area.eRecipe) {
            selectorGridPosition.y = 0;
            if (selectorGridPosition.x < areaGrids[Area.eRecipe].Size().width / 2) {
              currentArea = Area.eInventory;
              selectorGridPosition.x = Mathf.Max(0, Mathf.Min(areaGrids[Area.eInventory].Size().width - 1, selectorGridPosition.x));
            }
            else {
              currentArea = Area.eCrafting;
              selectorGridPosition.x = Mathf.Max(0, Mathf.Min(areaGrids[Area.eCrafting].Size().width - 1, selectorGridPosition.x - areaGrids[Area.eRecipe].Size().width / 2));
            }
          }
          else {
            selectorGridPosition.y--;
          }
        }
      }
    }

    Vector2 areaPosition = areaPositions[currentArea];
    targetPosition = new Vector3(areaPosition.x + tileSize.x * selectorGridPosition.x, areaPosition.y - tileSize.y * selectorGridPosition.y, -0.1f);

    float remainingDistance = (selector.transform.position - targetPosition).sqrMagnitude;
    if (remainingDistance > 0.001f) {
      moving = true;
      selector.transform.position = Vector3.MoveTowards(selector.transform.position, targetPosition, inverseMoveTime * Time.deltaTime);
    }
    else {
      moving = false;
      DropItem();
      PickUpItem();
    }
  }

  void PickUpItem() {
    if (pickUpItem) {
      Item item = areaGrids[currentArea].PopFromCell(selectorGridPosition);
      if (item != null) {
        item.transform.parent = selector.transform;
      }
    }
    pickUpItem = false;
  }

  void QueuePickUpItem() {
    pickUpItem = true;
  }

  void DropItem() {
    if (dropItem && selector.transform.childCount > 0) {
      Item item = selector.transform.GetChild(0).GetComponent<Item>();
      Debug.Assert(item != null);
      Grid currentGrid = areaGrids[currentArea];
      if (currentGrid.Cell(selectorGridPosition) == null) {
        currentGrid.DropOnCell(selectorGridPosition, item);
        item.transform.parent = this.transform;
      }
    }
    dropItem = false;
  }

  void QueueDropItem() {
    dropItem = true;
  }

  public override void BeginUse(PlayerCrewMan crew)
  {
    base.BeginUse(crew);
    if (crew.GetItem() != null) {
      print("Adding item to the list");
      crew.DropItem(this);
      crew.TryStopInteract();
    }
    else {
      print("Opening the craft window");
      craftingPort = GameManager.instance.viewportManager.OpenViewport(Viewport.ViewportTypes.Crafting, GameManager.instance.playerViewportLocations[crew.ControllerId]) as CraftingViewport;
      //craftingPort.terminalInstance = this;
    }
  }

  public override void EndUse(PlayerCrewMan crew)
  {
    base.EndUse(crew);
    GameManager.instance.viewportManager.CloseViewport(GameManager.instance.playerViewportLocations[crew.ControllerId]);
  }

  public override InteractableType GetInteractableType()
  {
    return InteractableType.CraftingTerminal;
  }

  public override void Interact(AICrewMan crew)
  {
    // Not implemented
  }

  public override void BeginUse(AICrewMan crew)
  {
    base.BeginUse(crew);

    if (crew.GetItem() != null)
    {
      print("Adding item to the list");
      crew.DropItem(this);
      crew.TryStopInteract();
    }
  }

  public override void EndUse(AICrewMan crew)
  {
    base.EndUse(crew);
    // Not implemented
  }

  public override List<JobType> GetJobs()
  {
    return new List<JobType>();
  }

  public override int GetJobPriority(JobType jobType)
  {
    return 0;
  }

  public bool AddItemToInventory(Item item) {
    GridPosition? addPosition = inventoryGrid.GetFirstOpenCell();

    if (addPosition.HasValue) {
      // Instantiate a new item and place it in the empty cell.
      Vector2 inventoryPosition = areaPositions[Area.eInventory];
      item.transform.parent = null;
      item.transform.localPosition = new Vector3(inventoryPosition.x + addPosition.Value.x * tileSize.x, inventoryPosition.y - addPosition.Value.y * tileSize.y, -0.1f);
      item.transform.localScale = Vector3.one;
      item.gameObject.layer = (int)Layer.CraftView;
      inventoryGrid.DropOnCell(addPosition.Value, item);
      print("Adding an item in position " + addPosition.Value + " of the inventory");
      return true;
    }
    else {
      return false;
    }
  }

  public Item FindAvailableItem(ItemType itemType)
  {
    throw new NotImplementedException();
  }

  public Item FindItem(ItemType itemType)
  {
    throw new NotImplementedException();
  }

  public bool RemoveItem(Item item)
  {
    throw new NotImplementedException();
  }
}
