﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Size
{
  public int width;
  public int height;

  public Size(int _width, int _height) {
    width = _width;
    height = _height;
  }

  public override string ToString() {
    return "(" + width + ", " + height + ")";
  }
}

public struct GridPosition
{
  public int x;
  public int y;

  public GridPosition(int _x, int _y) {
    x = _x;
    y = _y;
  }

  public override string ToString() {
    return "(" + x + ", " + y + ")";
  }
}

public class Grid
{
  Size _size;
  Item[,] _grid;

  public Grid(int width, int height) {
    _size.width = width;
    _size.height = height;
    _grid = new Item[_size.width, _size.height];
  }

  public Size Size() {
    return _size;
  }

  public ItemType CellItemTag(int x, int y) {
    if (_grid[x, y] != null) {
      return _grid[x, y].itemType;
    }
    return ItemType.None;
  }

  public Item Cell(GridPosition position) {
    return _grid[position.x, position.y];
  }

  public void DropOnCell(GridPosition position, Item obj) {
    _grid[position.x, position.y] = obj;
  }

  public GridPosition? GetFirstOpenCell() {
    GridPosition? addPosition = null;
      // go through all the positions in the grid and check if there
      // is an empty list to place the new item.
    foreach (GridPosition position in Cells()) {
      Item item = Cell(position);
      if (item == null) {
        addPosition = position;
        break;
      }
    }
    // Return the addPosition. Will be null if item could not be pushed.
    return addPosition;
  }

  public Item PopFromCell(GridPosition position) {
    Item cell = _grid[position.x, position.y];
    if (cell != null) {
      Item obj = _grid[position.x, position.y];
      _grid[position.x, position.y] = null;
      return obj;
    }
    else {
      Debug.Log("THIS");
      return null;
    }
  }

  // A generator that returns all the positions of the cells
  // in the grid first by rows, then columns.
  public IEnumerable<GridPosition> Cells() {
    for (int j = 0; j < _size.height; ++j) {
      for (int i = 0; i < _size.width; ++i) {
        yield return new GridPosition(i, j);
      }
    }
  }
}
