﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
  Vector2 previousPos;

  // Use this for initialization
  void Start()
  {
    previousPos = transform.position;
  }

  // Update is called once per frame
  void Update()
  {
  }


  GameObject target;
  Vector2 offset;
  bool missShot;

  public void FireAt(GameObject target, Vector2 offset)
  {
    this.target = target;
    this.offset = offset;
    transform.position += new Vector3(0, 0, -2);
    GetComponent<Rigidbody2D>().velocity = ((Vector2)target.transform.position + offset - (Vector2)transform.position).normalized * 15;

    // Shot knowns on init whether or not it will miss
    missShot = (Random.Range(0, 4) == 0);
  }

  void FixedUpdate()
  {
    Vector2 traveled = ((Vector2)transform.position - previousPos);
    RaycastHit2D hit = Physics2D.Raycast(previousPos, traveled, traveled.magnitude, 1 << target.layer);

    if (!missShot)
    {
      if (hit.collider != null)
      {
        if (hit.collider.CompareTag("Shield"))
        {
          Vector2 hitRelative = hit.transform.position - transform.position;
          Shield shield = hit.collider.GetComponent<Shield>();
          if (shield.TakeDamage(20))
          {
            Destroy(gameObject);
          }
        }
      }

      Vector2 relative = (Vector2)target.transform.position + offset - (Vector2)transform.position;

      if (relative.sqrMagnitude < 0.05f)
      {
        EvaluateHit();
      }
    }

    previousPos = transform.position;
  }

  void EvaluateHit()
  {
    Collider2D[] collisions = Physics2D.OverlapPointAll(transform.position, 1 << (int)Layer.Hull);
    bool hullHit = false;

    foreach (var collider in collisions)
    {
      if (collider.CompareTag("Hull"))
      {
        hullHit = true;
        break;
      }
    }

    if (hullHit)
    {
      Ship ship = target.GetComponent<Ship>();
      ship.TakeDamage(5);

      // Random chance to produce hull breach
      if (UnityEngine.Random.Range(0, 2) == 0)
      {
        TileNode hitTile = ship.GetTileForUnalignedPosition(transform.position);
        if (hitTile != null && !hitTile.HasBreach())
        {
          HullBreach breach = EntityManager.instance.InstantiateHullBreach();
          breach.transform.SetParent(hitTile.gameObject.transform);
          breach.transform.localPosition = new Vector3(0, 0, -0.2f);
          ship.AddBreach(breach, hitTile);
        }
      }

      Destroy(gameObject);
    }
  }

}
