﻿using UnityEngine;
using System.Collections;

public class Shield : MonoBehaviour
{
  public float _hitPoints = 100;

  public float hitPoints {
    get
    {
      return _hitPoints;
    }

    private set
    {
      _hitPoints = Mathf.Clamp(value, 0, 100);

      if (_hitPoints <= 0)
      {
        gameObject.SetActive(false);
      }
    }
  }

  public bool TakeDamage(float amount)
  {
    hitPoints -= amount;
    if (hitPoints <= 0)
    {
      return false;
    }
    return true;
  }

  public void Restore(float amount)
  {
    hitPoints += amount;
    gameObject.SetActive(true);
  }

  void Update()
  {
    TakeDamage(Time.deltaTime * 15);
  }
}
