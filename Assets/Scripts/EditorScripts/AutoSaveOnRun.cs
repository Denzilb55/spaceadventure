﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;


[InitializeOnLoad]
public class AutoSaveOnRun : MonoBehaviour
{
  static AutoSaveOnRun()
  {
    EditorApplication.playmodeStateChanged = () =>
    {
      if (EditorApplication.isPlayingOrWillChangePlaymode && !EditorApplication.isPlaying)
      {
        Debug.Log("Auto-Saving open scenes");
        EditorSceneManager.SaveOpenScenes();
        AssetDatabase.SaveAssets();
      }
    };
  }
}
#endif