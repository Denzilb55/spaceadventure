﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public interface IInventory
{
  bool AddItemToInventory(Item item);

  Item FindAvailableItem(ItemType itemType);

  Item FindItem(ItemType itemType);

  bool RemoveItem(Item item);
}

