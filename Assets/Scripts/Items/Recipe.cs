﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class RecipeBuilder
{
  protected Recipe _recipe;

  public RecipeBuilder() {
    build();
  }

  public void build() {
    _recipe = new Recipe();
    setBasicInfo();
    setResourcePositions();
  }

  abstract public void setBasicInfo();
  abstract public void setResourcePositions();

  public Recipe getRecipe() {
    return _recipe;
  }
}

public class PowerCellBuilder : RecipeBuilder
{
  public PowerCellBuilder() : base() { }

  public override void setBasicInfo() {
    _recipe.Name = "Power Cell";
    _recipe.ResourcesName = "ItemPowerCell";
    _recipe.Type = ItemType.EnergyCell;
  }

  public override void setResourcePositions() {
    _recipe.ItemGridPositions =  new ItemType[,]
    {{ItemType.None, ItemType.None, ItemType.None},
    {ItemType.None, ItemType.Wood, ItemType.None},
    {ItemType.None, ItemType.Wood, ItemType.None}};
  }
}

public class AdvancedPowerCellBuilder : RecipeBuilder
{
  public AdvancedPowerCellBuilder() : base() { }

  public override void setBasicInfo() {
    _recipe.Name = "Advanced Power Cell";
    _recipe.ResourcesName = "ItemAdvancedPowerCell";
    _recipe.Type = ItemType.AdvancedEnergyCell;
  }

  public override void setResourcePositions() {
    _recipe.ItemGridPositions =  new ItemType[,]
    {{ItemType.None, ItemType.Wood, ItemType.None},
    {ItemType.None, ItemType.Wood, ItemType.None},
    {ItemType.None, ItemType.Wood, ItemType.None}};
  }
}

public class Recipe
{
  string _name;
  ItemType _type;
  string _resourcesName;
  public ItemType[,] _resourceTagPositions;

  public Recipe() { }

  public Recipe(string name, ItemType type, string resourcesName) {
    _name = name;
    _type = type;
    _resourcesName = resourcesName;
  }

  public string Name {
    get { return _name; }
    set { _name = value; }
  }

  public ItemType Type {
    get { return _type; }
    set { _type = value; }
  }

  public ItemType[,] ItemGridPositions {
    get { return _resourceTagPositions; }
    set { _resourceTagPositions = value; }
  }

  public string ResourcesName {
    get { return _resourcesName; }
    set { _resourcesName = value; }
  }

  public bool CheckRecipe(Grid gridWithResources) {
    return false;
  }
}
