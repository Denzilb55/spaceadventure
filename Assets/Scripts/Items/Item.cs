﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game.Entities.Task;
using UnityEngine;

public enum ItemType
{
  None = -1,
  Wood,
  Stone,
  Ice,
  EnergyCell,
  AdvancedEnergyCell
}

// TODO: Consider making item interactable
public class Item : Interactable {

  public ItemType itemType;

  public override InteractableType GetInteractableType()
  {
    return InteractableType.Item;
  }

  // The job will be to put this away
  public override int GetJobPriority(JobType jobType)
  {
    return 1;
  }

  public override List<JobType> GetJobs()
  {
    return new List<JobType>();
  }

  public override void Interact(PlayerCrewMan crew)
  {
 
  }

  public override void Interact(AICrewMan crew)
  {

  }

  public override void BeginUse(PlayerCrewMan crew)
  {
    base.BeginUse(crew);
    crew.PickUpItem(this);
  }

  public override void BeginUse(AICrewMan crew)
  {
    base.BeginUse(crew);
    crew.PickUpItem(this);
  }

  public override void EndUse(PlayerCrewMan crew)
  {
    base.EndUse(crew);
    // crew.DropItem(crew.ship);
  }

  public override void EndUse(AICrewMan crew)
  {
    base.EndUse(crew);
   // crew.DropItem(ship);
  }

  // Use this for initialization
  void Start () {
    Debug.Assert(itemType != ItemType.None);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
