﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ShipItemRegistry : MonoBehaviour, IInventory
{
  private List<Item> items = new List<Item>();

  public bool AddItemToInventory(Item item)
  {
    items.Add(item);
    return true;
  }

  // Perhaps make it a dictionary of lists
  public Item FindItem(ItemType itemType)
  {
    foreach (Item item in items)
    {
      if (item.itemType == itemType)
      {
        return item;
      }
    }
    return null;
  }

  public Item FindAvailableItem(ItemType itemType)
  {
    foreach (Item item in items)
    {
      if (item.itemType == itemType && item.IsAvailableToAI())
      {
        return item;
      }
    }
    return null;
  }

  public bool RemoveItem(Item item)
  {
    return items.Remove(item);
  }
}

