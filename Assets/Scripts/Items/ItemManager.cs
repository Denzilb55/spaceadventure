﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour {

  public static ItemManager instance;
  public List<Item> items;

	// Use this for initialization
	void Awake () {
    Debug.Assert(instance == null);
    instance = this;
  }
	
	// Update is called once per frame
	void Update () {
		
	}
}
