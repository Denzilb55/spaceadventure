﻿using System;

public enum Layer
{
  Default = 0,
  CommanderView = 8,
  CraftView = 9,
  Team0 = 10,
  Team1 = 11,
  Team2 = 12,
  Team3 = 13,
  Hull  = 14,
}
