﻿using UnityEngine;
using System.Collections.Generic;

using Game.Room;
using Game.Ships;

public class EntityManager : MonoBehaviour
{
  public static EntityManager instance;

  public GameObject shipTemplate;
  public GameObject crewManTemplate;
  public HullBreach hullBreachTemplate;

  public PlayerCrewMan crewMan
  {
    get;
    private set;
  }

  public Ship playerShip
  {
    get;
    private set;
  }

  public List<Ship> otherShips = new List<Ship>();

  public List<Ship> allShips = new List<Ship>();

  public Color playerColour;
  public Color enemyColour;
  public Color neutralColour;

  private int maxSpaceIndex = 0;
  private const int SPACE_SIZE = 50;

  private List<Vector2> terminalPositions;

  void Awake()
  {
    // Unity singleton pattern
    if (instance == null)
    {
      instance = this;
    }
    else
    {
      Debug.LogError("Multiple EnityManager instances created!");
      Destroy(gameObject);
    }

    terminalPositions = new List<Vector2>();

    playerShip = InstantiateShip(playerColour, "USS Enterprise", Layer.Team0);
    allShips.Add(playerShip);

    GameManager.instance.playerViewportLocations.Add(1, ViewportManager.ViewportLocations.BottomLeft);
    GameManager.instance.playerViewportLocations.Add(2, ViewportManager.ViewportLocations.BottomRight);

    BaseCrewMan picard = InstantiatePlayerCrewMan("Picard", playerShip, Layer.Team0, Vector2.zero);
    picard.TakeControl(1);
    BaseCrewMan horse = InstantiateAICrewMan("Horse", playerShip, Layer.Team0, Vector2.down);
    BaseCrewMan Billy = InstantiateAICrewMan("Billy", playerShip, Layer.Team0, Vector2.down);
  //  BaseCrewMan donald = InstantiateAICrewMan("Donald", playerShip, Layer.Team0, Vector2.down);

    Physics2D.IgnoreCollision(picard.GetComponent<Collider2D>(), horse.GetComponent<Collider2D>());
    Physics2D.IgnoreCollision(Billy.GetComponent<Collider2D>(), horse.GetComponent<Collider2D>());
  //  Physics2D.IgnoreCollision(picard.GetComponent<Collider2D>(), Billy.GetComponent<Collider2D>());

  //  Physics2D.IgnoreCollision(donald.GetComponent<Collider2D>(), horse.GetComponent<Collider2D>());
  //  Physics2D.IgnoreCollision(donald.GetComponent<Collider2D>(), picard.GetComponent<Collider2D>());
  //  Physics2D.IgnoreCollision(donald.GetComponent<Collider2D>(), Billy.GetComponent<Collider2D>());


    Ship enemyShip = InstantiateShip(enemyColour, "Enemy", Layer.Team1);
    otherShips.Add(enemyShip);
    allShips.Add(enemyShip);
    BaseCrewMan spock = InstantiateAICrewMan("Spock", enemyShip, Layer.Team1, Vector2.zero);
    BaseCrewMan llama = InstantiateAICrewMan("Llama", enemyShip, Layer.Team1, Vector2.down);
    Physics2D.IgnoreCollision(spock.GetComponent<Collider2D>(), llama.GetComponent<Collider2D>());
  }

  /// <summary>
  /// Instantiates a ship.
  /// 
  /// Makes both a (main) space and local simulation copy for resolving local on-board
  /// physics, and links them.
  /// 
  /// The local ship version does not contain a ship component.
  /// </summary>
  /// <param name="cvColor">The Commander View icon colour</param>
  /// <param name="shipName">The name of the ship</param>
  /// <param name="position">The original position of the ship</param>
  /// <returns>Returns the created space ship (main copy)</returns>
  public Ship InstantiateShip(Color cvColor, string shipName, Layer teamLayer)
  {
    Debug.Log("Instantiating ship: " + shipName);

    Ship ship = Instantiate(shipTemplate).GetComponent<Ship>();
    ship.cvShip.color = cvColor;
    ship.name = shipName;

    // Offset to prevent artifacts at negatives
    ship.transform.position = new Vector2(maxSpaceIndex++ * SPACE_SIZE + 1000, 1000);
    SetDefaultLayerRecursively(ship.gameObject, (int)teamLayer);

    foreach (Room room in ship.GetComponentsInChildren<Room>())
    {
      room.SetOwner(ship);
    }

    ship.gameObject.AddComponent<ShipAI>();

    return ship;
  }

  public HullBreach InstantiateHullBreach()
  {
    HullBreach breach = Instantiate(hullBreachTemplate);
    return breach;
  }

  /// <summary>
  /// Instantiates a crewman.
  /// 
  /// Creates both a (main) and local simulation copy and links them.
  /// The local simulation copy is for resolving on-board physics, and
  /// the main copy is the rendered, absolute copy of the crewman.
  /// 
  /// Both the local and main crewman contains a CrewMan component.
  /// They are each marked with the "isLocalCopy" flag.
  /// </summary>
  /// <param name="name">The name of the crewman</param>
  /// <param name="ship">The ship on which to create the crewman</param>
  /// <returns>The main instance of the crewman</returns>
  public PlayerCrewMan InstantiatePlayerCrewMan(string name, Ship ship, Layer teamLayer, Vector2 offset)
  {
    PlayerCrewMan crewMan = Instantiate(crewManTemplate).AddComponent<PlayerCrewMan>();
    crewMan.name = name;
    crewMan.BoardShip(ship);
    crewMan.transform.localPosition += (Vector3)offset;
    SetDefaultLayerRecursively(crewMan.gameObject, (int)teamLayer);

    return crewMan;
  }

  public AICrewMan InstantiateAICrewMan(string name, Ship ship, Layer teamLayer, Vector2 offset)
  {
    AICrewMan crewMan = Instantiate(crewManTemplate).AddComponent<AICrewMan>();
    crewMan.name = name;
    crewMan.BoardShip(ship);
    crewMan.transform.localPosition += (Vector3)offset;
    SetDefaultLayerRecursively(crewMan.gameObject, (int)teamLayer);

    return crewMan;
  }

  void SetDefaultLayerRecursively(GameObject obj, int layer)
  {
    if (obj.layer == (int)Layer.Default)
    {
      obj.layer = layer;

      foreach (Transform child in obj.transform)
      {
        SetDefaultLayerRecursively(child.gameObject, layer);
      }
    }
  }

  public int RegisterTerminal(ref Vector2 position) {
    int value = terminalPositions.Count;
    Vector2 newPosition = new Vector2(0.0f, 10.0f * value);
    terminalPositions.Add(newPosition);
    position = newPosition;
    return value;
  }

  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {

  }
}
