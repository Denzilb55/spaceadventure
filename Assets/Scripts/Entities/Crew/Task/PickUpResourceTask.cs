﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game.Entities.Task
{
  public class PickUpResourceTask : AITask
  {
    private Ship ship;

    // TODO: fix, item needs to know on which ship it is
    public PickUpResourceTask(Interactable interactable, Ship ship, AITask followUpTask) : base(interactable, followUpTask)
    {
      this.ship = ship;
    }

    public override TileNode GetTaskTile()
    {
      return ship.GetTileForUnalignedPosition(interactable.transform.position);
    }

    public override TaskType GetTaskType()
    {
      return TaskType.PickUpResource;
    }

    public override AITask DoTask()
    {
      assignedCrewMan.TryInteract(interactable);
      // TODO: Handle in item
      assignedCrewMan.ship.RemoveItem(interactable as Item);
      return null;
    }
  }
}
