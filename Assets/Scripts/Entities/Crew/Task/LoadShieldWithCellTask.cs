﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Game.Entities.Task
{
  public class LoadShieldWithCellTask : AITask
  {
    ShieldTerminal shieldTerminal;

    public LoadShieldWithCellTask(ShieldTerminal shieldTerminal) : base(shieldTerminal)
    {
      this.shieldTerminal = shieldTerminal;
    }

    public override TileNode GetTaskTile()
    {
      return assignedCrewMan.tileNode;
    }

    public override TaskType GetTaskType()
    {
      return TaskType.LoadShieldWithCell;
    }

    public override bool RequiresItem(ItemType itemType)
    {
      if (itemType == ItemType.EnergyCell || itemType == ItemType.AdvancedEnergyCell)
      {
        return true;
      }

      return false;
    }

    public override AITask DoTask()
    {
      Item item = assignedCrewMan.GetItem();

      AITask dropOffTask = new DropItemInContainerTask(shieldTerminal, null);

      // If crewman is already holding a cell, just go drop it in the terminal
      if (item != null && shieldTerminal.IsItemRequired(item.itemType))
      {
        return dropOffTask;
      }
      else
      {
        Item foundItem = assignedCrewMan.ship.FindAvailableItem(ItemType.AdvancedEnergyCell);
        if (foundItem == null)
        {
          foundItem = assignedCrewMan.ship.FindAvailableItem(ItemType.EnergyCell);
        }

        if (foundItem != null)
        {
          return new PickUpResourceTask(foundItem, assignedCrewMan.ship, dropOffTask);
        }
        else
        {
          assignedCrewMan.ship.shipAI.taskBoard.AddJob(JobType.CraftEnergyCell, 15);
          return null; 
        }
      }
    }
  }
}
