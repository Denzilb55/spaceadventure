﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Game.Entities.Task
{
  public class CraftEnergyCellTask : AITask
  {
    CraftingTerminal craftingTerminal;
    ResouceDispenser dispencer;

    public CraftEnergyCellTask(CraftingTerminal craftingTerminal, ResouceDispenser dispencer) : base(craftingTerminal)
    {
      this.craftingTerminal = craftingTerminal;
      this.dispencer = dispencer;
    }

    bool ready = false;
    public override TileNode GetTaskTile()
    {
      Item item = assignedCrewMan.GetItem();

      if (item != null)
      {
        AssignNewInteractable(craftingTerminal);
        return craftingTerminal.GetTileNode();
      }
      else
      {
        if (craftingTerminal.ContainsIngredientsToCraft(ItemType.EnergyCell))
        {
          ready = true;
          AssignNewInteractable(craftingTerminal);
          return craftingTerminal.GetTileNode();
        }
        AssignNewInteractable(dispencer);
        return dispencer.GetTileNode();
      }
    }

    public override TaskType GetTaskType()
    {
      return TaskType.CraftEnergyCell;
    }

    public override bool RequiresItem(ItemType itemType)
    {
      if (itemType == ItemType.Wood)
      {
        return true;
      }

      return false;
    }

    protected override void OnStartTask()
    {
      assignedCrewMan.ship.shipAI.taskBoard.RemoveJob(JobType.CraftEnergyCell);
    }

   // public override bool ReEvaluate()
   // {
    //  return true;
    //}

    public override AITask DoTask()
    {
      if (!ready)
      {
        assignedCrewMan.TryInteract(interactable);
        return this;
      }
      else
      {
        if (craftingTerminal.ContainsIngredientsToCraft(ItemType.EnergyCell))
        {
          Item energyCell = craftingTerminal.CraftItemFromIngredients(ItemType.EnergyCell, assignedCrewMan as AICrewMan);
          if (energyCell != null)
          {
            assignedCrewMan.PickUpItem(energyCell);
          }

          // Requires re-evaluation because we do not know which shield terminal to make this
          // for, if any. We could probably pass it in as the shield terminal was likely to give
          // this task, but due to the longevity of this task a re-evaluation is probably not
          // a bad idea.
          return null;
        }
        else
        {
          ready = false;
          return this;
        }
        
      }      
    }
  }
}
