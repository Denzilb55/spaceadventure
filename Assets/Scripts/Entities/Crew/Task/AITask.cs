﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Game.Entities.Task
{
  public enum TaskType
  {
    Shield,
    Shoot,
    CraftEnergyCell,
    LoadShieldWithCell,
    PickUpResource,
    Repair,
    DropItemInContainer,
    Nothing,
  }

  public abstract class AITask
  {
    protected Interactable interactable
    {
      get;
      private set;
    }
    protected TileNode node;
    protected AICrewMan assignedCrewMan;

    protected AITask followUpTask;

    // Use this constructor for once off task with follow-up or null (no follow up)
    public AITask(Interactable interactable, AITask followUpTask)
    {
      this.interactable = interactable;
      followUpTask = this;
    }

    // Use this contructor for continuous task (TODO consider if this should actually be a thing)
    public AITask(Interactable interactable)
    {
      this.interactable = interactable;
      followUpTask = this;
    }

    protected void AssignNewInteractable(Interactable newInteractable)
    {
      if (interactable != newInteractable)
      {
        if (interactable != null)
        {
          interactable.Unflag(assignedCrewMan);
          if (interactable.IsOccupiedBy(assignedCrewMan))
          {
            interactable.EndUse(assignedCrewMan);
          }
        }
        interactable = newInteractable;
        interactable.Flag(assignedCrewMan);
      }
    }

    public virtual TileNode GetTaskTile()
    {
      return interactable.GetTileNode();
    }

    // Is the task currently completable in terms of available
    // interactable objects, resources and other requirements
    public virtual bool isCompletable()
    {
      return true;
    }

    public BaseCrewMan GetAssignedCrewMan()
    {
      return assignedCrewMan;
    }

    public bool IsAssigned()
    {
      return assignedCrewMan != null;
    }

    private void AssignCrewMan(AICrewMan crewMan)
    {
      assignedCrewMan = crewMan;
      if (interactable != null)
      {
        interactable.Flag(crewMan);
      }
    }

    public void StopTaskAndUnassignCrewMan()
    {
      StopTask();
      UnassignCrewMan();
    }

    public void UnassignCrewMan()
    {
      if (interactable != null)
      {
        interactable.Unflag(assignedCrewMan);
      }
      assignedCrewMan.OnUnassigned();
      assignedCrewMan = null;
    }

    // Returns a follow up task
    public virtual AITask DoTask()
    {
      assignedCrewMan.TryInteract(interactable);

      return followUpTask;
    }

    public abstract TaskType GetTaskType();

    public void StartTask(AICrewMan crewMan)
    {
      AssignCrewMan(crewMan);
      OnStartTask();
      Debug.Log(crewMan.name + " starts task " + this);
    }

    protected virtual void OnStartTask()
    {

    }

    public virtual bool RequiresItem(ItemType itemType)
    {
      return false;
    }

    public virtual void StopTask()
    {
      if (interactable && interactable.interactingCrewMan == assignedCrewMan)
      {
        interactable.EndUse(assignedCrewMan as AICrewMan);
      }
      assignedCrewMan.OnUnassigned();
    }

    // Check if task is still available
    public virtual bool ReEvaluate()
    {
      if (interactable == null)
      {
        return false;
      }

      if (interactable.IsOccupiedByOtherThan(assignedCrewMan))
      {
        Debug.Log(interactable + " is occupied");
        return false;
      }
      return true;
    }
  }
}
