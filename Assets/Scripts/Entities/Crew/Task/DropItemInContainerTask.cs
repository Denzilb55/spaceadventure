﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Game.Entities.Task
{
  // Drop any generic item in generic container interactable
  class DropItemInContainerTask : AITask
  {
    public DropItemInContainerTask(Interactable interactable, AITask followUpTask = null) : base(interactable, followUpTask)
    {
    }

    public override TaskType GetTaskType()
    {
      return TaskType.DropItemInContainer;
    }

    public override bool RequiresItem(ItemType itemType)
    {
      // For dropping task always assume that the container is correct?
      return true;
    }

    public override bool ReEvaluate()
    {
      return true;
    }
  }
}
