﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game.Entities.Task
{
  public class ShootTask : AITask
  {
    public ShootTask(Interactable interactable) : base(interactable)
    {
    }

    public override TaskType GetTaskType()
    {
      return TaskType.Shoot;
    }

    public override AITask DoTask()
    {
      interactable.BeginUse(assignedCrewMan as AICrewMan);
      interactable.Interact(assignedCrewMan as AICrewMan);
      interactable.EndUse(assignedCrewMan as AICrewMan);
      return null;
    }
  }
}
