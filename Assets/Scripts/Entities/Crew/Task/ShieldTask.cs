﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Game.Entities.Task
{
  public class ShieldTask : AITask
  {
    public ShieldTask(Interactable interactable) : base(interactable)
    {
    }

    public override TaskType GetTaskType()
    {
      return TaskType.Shield;
    }

    public override AITask DoTask()
    {
      interactable.BeginUse(assignedCrewMan as AICrewMan);
      interactable.Interact(assignedCrewMan as AICrewMan);
      interactable.EndUse(assignedCrewMan as AICrewMan);
      return null;
    }
  }
}
