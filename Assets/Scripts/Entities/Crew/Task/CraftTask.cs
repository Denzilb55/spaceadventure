﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game.Entities.Task
{
  public class CraftTask : AITask
  {
    public CraftTask(Interactable interactable) : base(interactable)
    {
    }

    public override TaskType GetTaskType()
    {
      return TaskType.CraftEnergyCell;
    }
  }
}
