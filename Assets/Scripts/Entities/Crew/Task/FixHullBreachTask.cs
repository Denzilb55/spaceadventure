﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Game.Ships;

namespace Game.Entities.Task
{
  public class FixHullBreachTask : AITask
  {

    public FixHullBreachTask(Interactable breach) : base(breach)
    {
      
    }

    public override AITask DoTask()
    {
      interactable.BeginUse(assignedCrewMan as AICrewMan);
      interactable.Interact(assignedCrewMan as AICrewMan);
      interactable.EndUse(assignedCrewMan as AICrewMan);
      return null;
    }

    public override void StopTask()
    {
      
      base.StopTask();
    }

    public override TaskType GetTaskType()
    {
      return TaskType.Repair;
    }
  }
}
