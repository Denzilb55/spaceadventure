﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game.Entities.Task
{
  // A job is the "main" job that needs to be done. For instance,
  // "make ammo" or "shield". The ship knows the overall priority
  // for these jobs based on its own state / stats and the state of
  // its components.
  //
  // Each job will consist of at least one task, or perhaps a graph
  // of tasks. It may be a simple series of tasks, or tasks that can
  // be done in parallel (or a combination of both).
  //
  // A job's task structure will also not necessarily be fixed,
  // and will be modified as requirements change (for instance, in order
  // to shoot, we need to produce ammo if there is not ammo, and if the
  // crafting machine is broken this needs to be fixed first, etc, etc).
  //
  // (to consider) Different jobs may have overlapping tasks, such as
  // produce component, in which case that task type's priority will
  // be the combination of both.
  //  
  public enum JobType
  {
    None,
    Attack,
    Shield,
    Repair,

    LoadShieldWithCell,
    CraftEnergyCell
  }

  public interface IJobLister
  {
    // Get the list of jobs that this lister gives.
    List<JobType> GetJobs();

    // Get the priority for the job. Perhaps move outside to ShipAI?
    int GetJobPriority(JobType jobType);

    // This function must be manually called at the
    // appropriate moment from each JobLister. It will
    // likely be called internally, so is added to the
    // interface as a reminder to implement this method
    void RegisterWithShipAI();
  }
}
