﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Game.Ships;

namespace Game.Entities.Task
{
  // A place to hook up persistent tasks
  public class TaskBoard : IJobLister
  {
    private Dictionary<JobType, int> tasks = new Dictionary<JobType, int>();
    private ShipAI shipAi;

    public TaskBoard(ShipAI shipAi)
    {
      this.shipAi = shipAi;
      shipAi.RegisterJobLister(this);
    }

    public void AddJob(JobType jobType, int priority)
    {
      int currentValue = 0;

      Debug.Log("List task on board: " + jobType + " " + priority);

      if (tasks.TryGetValue(jobType, out currentValue))
      {
        int newVal = Math.Min(currentValue + priority, 100);

        tasks[jobType] = newVal;
      }
      else
      {
        tasks.Add(jobType, priority);
      }
    }

    public void RemoveJob(JobType jobType)
    {
      tasks.Remove(jobType);
      Debug.Log("Unlist task on board: " + jobType);
    }

    public int GetJobPriority(JobType jobType)
    {
      Debug.Assert(tasks.ContainsKey(jobType));
      Debug.Log("PRIORTIY " + tasks[jobType] + " " + jobType);
      return tasks[jobType];
    }

    public List<JobType> GetJobs()
    {
      return tasks.Keys.ToList();
    }

    public void RegisterWithShipAI()
    {
      shipAi.RegisterJobLister(this);
    }
  }
}
