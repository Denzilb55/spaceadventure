﻿using UnityEngine;
using System.Collections;

using Game.Room;
using Game.Ships;

public abstract class BaseCrewMan : MonoBehaviour
{
  public Rigidbody2D body;
  public bool inControl;
  public Rigidbody2D shipBody;
  public Ship ship;
  protected Ship mainShip;
  public float movementSpeed;

  // The room the player is currently in
  private Room currentRoom;

  private Item currentItem;

  protected ShipAI shipAi;

  // Use this for initialization
  void Start()
  {
    inControl = false;

    body = GetComponent<Rigidbody2D>();
    OnStart();
    movementSpeed = 3;
    shipAi = ship.GetComponent<ShipAI>();
  }

  protected abstract void OnStart();
  protected abstract void OnUpdate();

  // Update is called once per frame
  void Update()
  {
    if (interacting)
    {
      Interact(nearestInteractable);
    }

    OnUpdate();
  }

  protected abstract void Interact(Interactable i);
  protected abstract void BeginUse(Interactable i);
  protected abstract void EndUse(Interactable i);

  public virtual void OnUnassigned()
  {
    currentInteractable = null;
  }

  public void StopInteract()
  {
    currentInteractable = null;
  }

  public Vector2 gridPos
  {
    get
    {
      return new Vector2((int)(transform.position.x + 0.5f), (int)(transform.position.y + 0.5f));
    }
  }

  public TileNode tileNode
  {
    get
    {
      return ship.tiles[gridPos];
    }
  }

  void FixedUpdate()
  {
    if (interacting)
    {
      body.constraints = RigidbodyConstraints2D.FreezeAll;
    }
    else
    {
      body.constraints = RigidbodyConstraints2D.FreezeRotation;
    }
  }

  protected bool interacting
  {
    get { return currentInteractable != null; }
  }

  public Interactable currentInteractable
  {
    get;
    set; 
  }

  public Interactable nearestInteractable
  {
    get
    {
      return _getNearestInteractable();
    }
  }

  protected virtual Interactable _getNearestInteractable()
  {
    float dist = 100000;
    Interactable interactableToPickup = null;
    foreach (Interactable i in tileNode.gameObject.GetComponentsInChildren<Interactable>()) {
      float distanceToItem = Vector2.Distance(i.transform.position, transform.position);
      if (distanceToItem < dist)
      {
        dist = distanceToItem;
        interactableToPickup = i;
      }
    }

    return interactableToPickup;
  }

  public int ControllerId { get { return controllerId; } }
  private int controllerId = -1;
  public void TakeControl(int controllerId)
  {
    this.controllerId = controllerId;
  }

  public bool IsInOwnShip
  {
    get
    {
      return mainShip == ship;
    }
  }

  /// <summary>
  /// Transfers the crewman to another ship.
  /// 
  /// This transfers both the main and local copy.
  /// </summary>
  /// <param name="shipToBoard">The ship to board</param>
  public void BoardShip(Ship shipToBoard)
  {
    Debug.Log(name + " boarded ship " + shipToBoard.name);
    ship = shipToBoard;
    transform.SetParent(ship.transform, false);

    if (mainShip == null)
    {
      mainShip = shipToBoard;
    }
    else
    {
      if (shipToBoard != mainShip)
      {
        Viewport port = GameManager.instance.viewportManager.OpenViewport(Viewport.ViewportTypes.Space, GameManager.instance.playerViewportLocations[controllerId]);
        port.FocusTransform(this.transform);
      }
      else
      {
        GameManager.instance.viewportManager.CloseViewport(GameManager.instance.playerViewportLocations[controllerId]);
      }
    }

  }

  public void OnRoomEnter(Room room)
  {
    if (currentRoom != null)
    {
      OnRoomExit(currentRoom);
    }
    currentRoom = room;
  }

  public void OnRoomExit(Room room)
  {
    if (currentRoom == room)
    {
      currentRoom = null;
    }
  }

  protected void updateAnimation(Vector2 direction)
  {
    if (direction.sqrMagnitude > 0)
    {
      if (Mathf.Abs(direction.x) >= Mathf.Abs(direction.y))
      {
        if (direction.x > 0)
        {
          GetComponent<Animator>().SetInteger("Direction", 2);
        }
        else
        {
          GetComponent<Animator>().SetInteger("Direction", 4);
        }
      }
      else
      {
        if (direction.y > 0)
        {
          GetComponent<Animator>().SetInteger("Direction", 1);
        }
        else
        {
          GetComponent<Animator>().SetInteger("Direction", 3);
        }
      }
    }
  }

  public void PickUpItem(Item item) {
    Debug.Assert(currentItem == null);

    if (currentItem != null)
    {
      return;
    }
    print(name + " picks up " + item.itemType);
    currentItem = item;
    item.transform.parent = this.transform;
    item.transform.localPosition = new Vector3(0, 0, -2);
    item.transform.localScale = new Vector3(0.5f, 0.5f, 1.0f);
    item.gameObject.layer = this.gameObject.layer;
  }

  public Item GetItem() {
    return currentItem;
  }

  // Drop item into container
  public Item DropItem(IInventory container)
  {
    if (currentItem == null)
    {
      return null;
    }
    print(name + " drops " + currentItem.itemType);
    currentItem.transform.parent = null;
    Item item = currentItem;
    EndUse(currentItem);
    container.AddItemToInventory(item);
    currentItem = null;
    return item;
  }
}
