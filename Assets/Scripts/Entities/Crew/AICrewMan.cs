﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

using Game.Room;
using System;
using Game.Ships;
using Game.Entities.Task;

public class AICrewMan : BaseCrewMan
{

  class SearchNode
  {
    public SearchNode(TileNode tile, SearchNode parent, float cost)
    {
      this.tile = tile;
      this.parent = parent;
      this.cost = cost;
    }

    public TileNode tile;
    public SearchNode parent;
    public float cost;
  }

  protected override void OnStart()
  {

  }

  private Stack<TileNode> path;
  private Vector2 moveTarget;

  protected override void OnUpdate()
  {
    Think();

    if (!interacting)
    {
     // Pathfind();
    }
  }

  public AITask task
  {
    get;
    private set;
  }

  public override void OnUnassigned()
  {
    if (task != null)
    Debug.Log(name + " is assigned from " + task.GetTaskType());
    base.OnUnassigned();
    path = null;
    task = null;
  }

  private Interactable _interactable;
  protected override Interactable _getNearestInteractable()
  {
    return _interactable;
  }

  public void TryInteract(Interactable interactable)
  {
    Debug.Log(name + " tries interacting");
    if (interactable != null)
    {
      if (!interacting)
      {
        _interactable = interactable;
        BeginUse(interactable);
        body.velocity = Vector2.zero;
      }
    }
  }

  // Consider moving this to Player,
  // AI crew directly controlled by Tasks
  public void TryStopInteract()
  {
    if (interacting)
    {
      EndUse(nearestInteractable);
      _interactable = null;
    }

    StopInteract();
  }

  void Think()
  {
    // For now, only accept task if not currently doing task.
    // TODO: implement reprioritising for handling emergencies
    if (task == null)
    {
      AITask newTask = shipAi.GetPriorityTask(this);
      if (newTask != task && newTask != null)
      {
        task = newTask;
        task.StartTask(this);
      }
    }
    else
    {
      if (!task.ReEvaluate())
      {
        task.StopTaskAndUnassignCrewMan();
      }
    }


    if (task != null)
    {
      Pathfind(task.GetTaskTile());

      if (atTarget)
      {
        AITask nextTask = task.DoTask();

        if (nextTask != task)
        {
          Debug.Log(name + " STOP AND UNASSIGN, DONE!");
          task.StopTaskAndUnassignCrewMan();
          task = nextTask;

          if (task != null)
          {
            task.StartTask(this);
          }
        } 
      }
    }

    if (task != null)
    {
      // If AI is carrying an item that it does not require,
      // throw it to the ground!
      if (GetItem() != null && !task.RequiresItem(GetItem().itemType))
      {
        print(name + " throws it to the ground");
        DropItem(ship);
      }
    }

  }

  bool atTarget = false;
  void Pathfind(TileNode pathfindTarget)
  {
    atTarget = false;
    if (pathfindTarget == tileNode)
    {
      atTarget = true;
      body.velocity = Vector2.zero;
      path = null;
      return;
    }

    if (path == null || path.Count == 0)
    {
      path = null;
      StartSearch(tileNode, pathfindTarget);
    }
    else
    {
      body.velocity = Vector2.zero;
      TileNode targetNode = path.Peek();

      if (targetNode != null)
      {
        moveTarget = targetNode.gridPos;

        Vector2 relative = moveTarget - (Vector2)transform.position;
        body.velocity = (moveTarget - (Vector2)transform.position).normalized * movementSpeed;
        updateAnimation(body.velocity);
        if (relative.sqrMagnitude < 0.2f)
        {
          path.Pop();

          if (path.Count == 0)
          {
            path = null;
            atTarget = true;
            body.velocity = Vector2.zero;
          }
        }
      }
    }
  }

  protected override void Interact(Interactable i)
  {
    i.Interact(this);
  }

  protected override void BeginUse(Interactable i)
  {
    i.BeginUse(this);
  }

  protected override void EndUse(Interactable i)
  {
    i.EndUse(this);
  }

  private void StartSearch(TileNode startNode, TileNode targetNode)
  {
    SearchNode baseNode = new SearchNode(startNode, null, 0);
    ExploreNode(baseNode, targetNode);

    while (openList.Count > 0 && countout++ < 100)
    {
      SearchNode backPath = ExploreNode(openList[0], targetNode);

      openList = openList.OrderBy(x => x.cost).ToList();

      if (backPath != null)
      {
        path = new Stack<TileNode>();

        while (backPath.parent != null)
        {
          path.Push(backPath.tile);
          backPath = backPath.parent;  
        }

        if (path.Count > 0)
        {
          atTarget = false;
        }

        break;
      }
    }

    countout = 0;

    closedList.Clear();
    openList.Clear();
  }

  int countout = 0;

  private void EvalNode(TileNode node, SearchNode parentNode)
  {
    if (node != null)
    {
      if (!closedList.ContainsKey(node))
      {
        SearchNode sn = new SearchNode(node, parentNode, parentNode.cost + 1);
        openList.Add(sn);
        closedList.Add(node, sn);
      }
      else
      {
        SearchNode sn = closedList[node];
        if (parentNode.cost + 1 < sn.cost)
        {
          sn.parent = parentNode;
          sn.cost = parentNode.cost + 1;
        }
      }
    }
  }

  private SearchNode ExploreNode(SearchNode node, TileNode targetNode)
  {
    openList.Remove(node);

    if (node.tile == targetNode)
    {
      return node;
    }


    EvalNode(node.tile.leftNode, node);
    EvalNode(node.tile.rightNode, node);
    EvalNode(node.tile.upNode, node);
    EvalNode(node.tile.downNode, node);

    return null;
  }

  private List<SearchNode> openList = new List<SearchNode>();
  private Dictionary<TileNode, SearchNode> closedList = new Dictionary<TileNode, SearchNode>();
}
