﻿using UnityEngine;
using System.Collections;

using Game.Room;
using Game.Entities.Task;
using System;

public class PlayerCrewMan : BaseCrewMan
{

  protected override void OnStart()
  {
 
  }

  protected override void OnUpdate()
  {
    if (!interacting)
    {
      Vector2 vel = new Vector2();
      if (cInput.GetKey("KBLeft1"))
      {
        vel.x = -1;
      }
      else if (cInput.GetKey("KBRight1"))
      {
        vel.x = 1;
      }
      if (cInput.GetKey("KBUp1"))
      {
        vel.y = 1;
      }
      else if (cInput.GetKey("KBDown1"))
      {
        vel.y = -1;
      }

      if (vel.sqrMagnitude > 0)
      {
        body.velocity = vel * movementSpeed;
      }
      else
      {
        body.velocity = new Vector2(cInput.GetAxis("HorisontalMovement" + ControllerId), cInput.GetAxis("VerticalMovement" + ControllerId)) * movementSpeed;
      }

      updateAnimation(body.velocity);
    }

    if (cInput.GetKeyDown("Primary" + ControllerId) || cInput.GetKey("KBPrimary" + ControllerId))
    {
      bool interacted = false;
      if (nearestInteractable != null)
      {
        if (!interacting)
        {
          // TODO: Player should mark interactables as unusable and
          // AI should be kicked off, but player should not 
          // be assigned to task

          currentInteractable = nearestInteractable;
          BeginUse(currentInteractable);

          body.velocity = Vector2.zero;
          interacted = true;
        }
      }

      if (!interacted)
      {

      }
    }
    else if (cInput.GetKeyDown("Secondary" + ControllerId) || cInput.GetKey("KBSecondary" + ControllerId))
    {
      if (interacting)
      {
        EndUse(nearestInteractable);
      }
      else if (GetItem() != null)
      {
        IInventory inventory = nearestInteractable as IInventory;
        if (inventory == null)
        {
          DropItem(ship);
        }
        else
        {
          DropItem(inventory);
        }
        
      }

      currentInteractable = null;
    }
  }

  public void DropItemOnSpot()
  { 
}

  protected override void Interact(Interactable i)
  {
    i.Interact(this);
  }

  protected override void BeginUse(Interactable i)
  {
    i.BeginUse(this);
  }

  protected override void EndUse(Interactable i)
  {
    i.EndUse(this);
  }

  public void TryInteract()
  {
    if (nearestInteractable != null)
    {
      if (!interacting)
      {
        // TODO: Player should mark interactables as unusable and
        // AI should be kicked off, but player should not 
        // be assigned to task

        currentInteractable = nearestInteractable;
        BeginUse(currentInteractable);

        body.velocity = Vector2.zero;
      }
    }
  }

  public void TryStopInteract()
  {
    if (interacting)
    {
      EndUse(nearestInteractable);
    }

    currentInteractable = null;
  }

}