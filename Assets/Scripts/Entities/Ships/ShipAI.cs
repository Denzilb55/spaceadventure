﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Entities.Task;
using System.Linq;

namespace Game.Ships {

  public class ShipAI : MonoBehaviour {

    private Dictionary<TaskType, int> taskTypePriorities;

    private List<IJobLister> jobListers = new List<IJobLister>();
    public TaskBoard taskBoard;

    private Ship ship;
    public void Init()
    {
      ship = GetComponent<Ship>();
      taskBoard = new TaskBoard(this);
    }

    public AITask GetPriorityTask(BaseCrewMan crewMan)
    {
      List<TaskType> priorityTaskList = taskTypePriorities.Keys.ToList();
      if (priorityTaskList.Count == 0)
      {
        Debug.Log("NO TASK");
        return null;
      }

      AITask priorityTask = null;

      priorityTaskList = priorityTaskList.OrderByDescending(d => taskTypePriorities[d]).ToList();


      foreach (TaskType taskType in priorityTaskList)
      {
        AITask task = GetTaskOfType(taskType);

        if (task != null)
        {
          Debug.Log("Highest priority task: " + taskType + " " + taskTypePriorities[taskType]);
          priorityTask = task;
          break;
        }
      }

      // No task found
      if (priorityTask == null)
      {
        return null;
      }

      // For now, just decrease the current priority
      taskTypePriorities[priorityTask.GetTaskType()] = (int)(0.5f * taskTypePriorities[priorityTask.GetTaskType()]);

      return priorityTask;
    }

    private bool IsJobAvailable()
    {
      return true;
    }

    public void RegisterJobLister(IJobLister jobLister)
    {
      jobListers.Add(jobLister);
    }

    public void UnRegisterJobLister(IJobLister jobLister)
    {
      jobListers.Remove(jobLister);
    }    

    // TODO: Consider making functional
    private void CalculateRecentTaskPriorities()
    {
      taskTypePriorities = new Dictionary<TaskType, int>();

      foreach (IJobLister jobLister in jobListers)
      {
        foreach (JobType jobType in jobLister.GetJobs())
        {
          int jobPriority = jobLister.GetJobPriority(jobType);
          switch (jobType)
          {
            case JobType.Attack:
              {
                IncreasePriority(TaskType.Shoot, jobPriority);
                break;
              }

            case JobType.Repair:
              {
                IncreasePriority(TaskType.Repair, jobPriority);
                break;
              }

            case JobType.Shield:
              {
                ShieldTerminal shieldTerminal = ship.GetInteractableOfType(InteractableType.ShieldTerminal) as ShieldTerminal;

              //  Debug.Log("UNAVAILABLE DUE TO REQUIRE ITEM: " + shieldTerminal.IsUnavailableDueTo(UnavailableCause.RequireItem));
               // Debug.Log("UNAVAILABLE DUE TO NOT RECHARGED: " + shieldTerminal.IsUnavailableDueTo(UnavailableCause.NotRecharged));
              //  Debug.Log("UNAVAILABLE DUE TO IN USE: " + shieldTerminal.IsUnavailableDueTo(UnavailableCause.InUse));

                if (ship.GetAvailableInteractableOfType(InteractableType.ShieldTerminal) != null)
                {
                  Debug.Log("ACTIVEATE SHIELD JOB " + ship.name);
                  IncreasePriority(TaskType.Shield, jobPriority);
                }

                break;
              }

            case JobType.CraftEnergyCell:
              {
                IncreasePriority(TaskType.CraftEnergyCell, jobPriority);
                break;
              }

            case JobType.LoadShieldWithCell:
              {
                IncreasePriority(TaskType.LoadShieldWithCell, jobPriority);
                break;
              }
          }
        }
      }
    }

    private void IncreasePriority(TaskType task, int priority)
    {
      int currentPriority = 0;

      if (!taskTypePriorities.TryGetValue(task, out currentPriority))
      {
        taskTypePriorities.Add(task, priority);
      }
      else
      {
        taskTypePriorities[task] = currentPriority + priority;
      }

   //   Debug.Log("TASK PRIORITY " + task + " " + taskTypePriorities[task]);
    }

    public AITask GetTaskOfType(TaskType type)
    {
      switch (type)
      {
        // So far each case looks like it may be very similar. Consider refactoring.
        case TaskType.Shield:
          {
            Interactable shieldTerminal = ship.GetAvailableInteractableOfType(InteractableType.ShieldTerminal);

            if (shieldTerminal != null)
            {
              return new ShieldTask(shieldTerminal);
            }

            break;
          }

        case TaskType.Shoot:
          {
            Interactable weaponsTerminal = ship.GetAvailableInteractableOfType(InteractableType.WeaponsTerminal);

            if (weaponsTerminal != null)
            {
              return new ShootTask(weaponsTerminal);
            }
            break;
          }

        case TaskType.Repair:
          {
            Interactable hullBreach = ship.GetAvailableInteractableOfType(InteractableType.HullBreach);

            if (hullBreach != null)
            {
              return new FixHullBreachTask(hullBreach);
            }
            break;
          }

        case TaskType.CraftEnergyCell:
          {
            // TODO: Check ship for existing cells

            ResouceDispenser dispencer = ship.GetAvailableDispencer(ItemType.Wood) as ResouceDispenser;
            CraftingTerminal craftingTerminal = ship.GetAvailableInteractableOfType(InteractableType.CraftingTerminal) as CraftingTerminal;

          //  Debug.Log("AVAILABLE DISPENCER: " + dispencer);
          //  Debug.Log("AVAILABLE CRAFTING: " + craftingTerminal);

            if ( dispencer != null && craftingTerminal != null)
            {
              return new CraftEnergyCellTask(craftingTerminal, dispencer);
            }
           // InteractableType 

            break;
          }

        case TaskType.LoadShieldWithCell:
          {
            ShieldTerminal shieldTerminal = ship.GetInteractableOfType(InteractableType.ShieldTerminal) as ShieldTerminal;

            if (shieldTerminal != null)
            {
              return new LoadShieldWithCellTask(shieldTerminal);
            }
            break;
          }
      }
      return null;
    }

    private void Update()
    {
      CalculateRecentTaskPriorities();
    }

  }
}