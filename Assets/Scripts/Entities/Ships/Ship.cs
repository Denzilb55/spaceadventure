﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Game.Ships;

using Game.Room;

public class Ship : MonoBehaviour, IInventory
{
  public Rigidbody2D body
  {
    get;
    private set;
  }

  public Transform hull;
  public Shield shield;
  public CVShip cvShip;
  private float turnSensitivity = 30000;
  private float engineStrength = 60000.0f;

  private float thrustSetting;
  private float torqueSetting;

  public Dictionary<Vector2, TileNode> tiles;
 // public List<IInteractable> interactables;

  public Dictionary<InteractableType, List<Interactable>> interactables;

  public List<Item> looseItems = new List<Item>();
  public List<Room> rooms;

  public Ship dockedShip;
  public ShipAI shipAI;
  private ShipItemRegistry itemRegistry;


  // Use this for initialization
  void Start()
  {
    body = GetComponent<Rigidbody2D>();
    itemRegistry = gameObject.AddComponent<ShipItemRegistry>();
    shipAI = GetComponent<ShipAI>();
    if (shipAI == null)
    {
      shipAI = gameObject.AddComponent<ShipAI>();
    }
    shipAI.Init();
    ConnectShip();    
  }

  public void AddBreach(HullBreach breach, TileNode node)
  {
    RegisterInteractable(breach, node);
  }

  public TileNode GetTileForUnalignedPosition(Vector2 pos)
  {
    Vector2 gridPos = new Vector2((int)(pos.x + 0.5f), (int)(pos.y + 0.5f));
    TileNode tile = null;
    tiles.TryGetValue(gridPos, out tile);

    return tile;
  }

  TileNode GetOrInitialiseNode(Transform tile)
  {
    TileNode node;
    Vector2 gridPos = new Vector2((int)tile.position.x, (int)tile.position.y);

    if (!tiles.TryGetValue(gridPos, out node))
    {
      node = new TileNode(tile.gameObject);
      tiles.Add(gridPos, node);
      Interactable interactable = tile.GetComponentInChildren<Interactable>();
      if (interactable != null)
      {
        RegisterInteractable(interactable, node);
      }
    }

    return node;
  }

  void RegisterInteractable(Interactable interactable, TileNode node)
  {
    InteractableType iType = interactable.GetInteractableType();
    if (!interactables.ContainsKey(iType))
    {
      interactables.Add(iType, new List<Interactable>());
    }

    interactables[iType].Add(interactable);

    interactable.Initialise(this, node);
    shipAI.RegisterJobLister(interactable);
  }

  public void NotifyInteractableDestroy(Interactable interactable)
  {
    interactables[interactable.GetInteractableType()].Remove(interactable);
    shipAI.UnRegisterJobLister(interactable);
  }

  public int GetInteractableCount(InteractableType type)
  {
    if (interactables.ContainsKey(type))
    {
      return interactables[type].Count;
    }
    return 0;
  }

  public Interactable GetAvailableDispencer(ItemType dispencedType)
  {
    foreach (Interactable interactable in interactables[InteractableType.ResourcesDispenser])
    {
      if (interactable.IsAvailableToAI())
      {
        if ((interactable as ResouceDispenser).itemTemplate.itemType == dispencedType)
        {
          return interactable;
        }        
      }
    }

    return null;
  }

  public Interactable GetAvailableInteractableOfType(InteractableType type)
  {
    foreach (Interactable interactable in interactables[type])
    {
      if (interactable.IsAvailableToAI())
      {
        return interactable;
      }
    }

    return null;
  }

  public Interactable GetInteractableOfType(InteractableType type)
  {
    foreach (Interactable interactable in interactables[type])
    {
      return interactable;
    }

    return null;
  }

  public Interactable GetUnavailableInteractableOfType(InteractableType type)
  {
    foreach (Interactable interactable in interactables[type])
    {
      if (!interactable.IsAvailableToAI())
      {
        return interactable;
      }
    }

    return null;
  }

  void ConnectShip()
  {
    Debug.Log("CONNECTING SHIP");
    tiles = new Dictionary<Vector2, TileNode>();
    interactables = new Dictionary<InteractableType, List<Interactable>>();

    // Do first pass to find all tiles and rooms
    foreach (Room room in GetComponentsInChildren<Room>())
    {
      room.SetOwner(this);
      rooms.Add(room);

      foreach (Transform tile in room.transform)
      {
        TileNode node = GetOrInitialiseNode(tile);
      }
    }

    foreach (KeyValuePair<Vector2, TileNode> posTilePair in tiles)
    {
      TileNode node = posTilePair.Value;
      Transform tile = posTilePair.Value.gameObject.transform;

     // Debug.Log("TESTING: " + tile.name + " from " + tile.parent.name);

      RaycastHit2D hitUp = Physics2D.Raycast(tile.transform.position, Vector2.up, 1, 1 << gameObject.layer);
      RaycastHit2D hitDown = Physics2D.Raycast(tile.transform.position, Vector2.down, 1, 1 << gameObject.layer);
      RaycastHit2D hitLeft = Physics2D.Raycast(tile.transform.position, Vector2.left, 1, 1 << gameObject.layer);
      RaycastHit2D hitRight = Physics2D.Raycast(tile.transform.position, Vector2.right, 1, 1 << gameObject.layer);

      if (!hitUp.collider || !hitUp.collider.CompareTag("Wall"))
      {
      //  Debug.Log("From: " + posTilePair.Key + " " + (posTilePair.Key + Vector2.up));
        node.upNode = tiles[posTilePair.Key + Vector2.up];
      }

      if (!hitDown.collider || !hitDown.collider.CompareTag("Wall"))
      {
      //  Debug.Log("From: " + posTilePair.Key + " " + (posTilePair.Key + Vector2.down));
        node.downNode = tiles[posTilePair.Key + Vector2.down];
      }

      if (!hitLeft.collider || !hitLeft.collider.CompareTag("Wall"))
      {
      //  Debug.Log("From: " + posTilePair.Key + " " + (posTilePair.Key + Vector2.left));
        node.leftNode = tiles[posTilePair.Key + Vector2.left];
      }

      if (!hitRight.collider || !hitRight.collider.CompareTag("Wall"))
      {
      //  Debug.Log("From: " + posTilePair.Key + " " + (posTilePair.Key + Vector2.right) );
        node.rightNode = tiles[posTilePair.Key + Vector2.right];
      }
    }
  }

  protected virtual void OnUpdate()
  {

  }

  // Update is called once per frame
  void Update()
  {
    OnUpdate();
  }

  public void Steer(float thrust, float torque)
  {
    thrustSetting = thrust;
    torqueSetting = torque;
  }

  private float hullStrength = 100;
  public void TakeDamage(float amount)
  {
    hullStrength -= amount;

    if (hullStrength <= 0)
    {
      // Causes crash, so don't destroy for now
      gameObject.SetActive(false);
    }
  }

  public void Repair(float amount)
  {
    hullStrength += amount;
  }

  void FixedUpdate()
  {
    body.AddTorque(torqueSetting * -turnSensitivity);
    body.AddForce(transform.up * thrustSetting * engineStrength);
  }


  void OnCollisionEnter2D(Collision2D collision)
  {
    if (collision.transform.CompareTag("Ship"))
    {
      dockedShip = collision.transform.GetComponent<Ship>();
    }
  }

  void OnCollisionExit2D(Collision2D collision)
  {
    if (collision.transform.CompareTag("Ship"))
    {
      dockedShip = null;
    }
  }

  public bool AddItemToInventory(Item item)
  {
    item.transform.SetParent(GetTileForUnalignedPosition(item.transform.position).gameObject.transform, true);
    return itemRegistry.AddItemToInventory(item);
  }

  public Item FindAvailableItem(ItemType itemType)
  {
    return itemRegistry.FindAvailableItem(itemType);
  }

  public Item FindItem(ItemType itemType)
  {
    return itemRegistry.FindItem(itemType);
  }

  public bool RemoveItem(Item item)
  {
    return itemRegistry.RemoveItem(item);
  }
}
