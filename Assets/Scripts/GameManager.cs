﻿using UnityEngine;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
  public ViewportManager viewportManager;

  public static GameManager instance;

  public Dictionary<int, ViewportManager.ViewportLocations> playerViewportLocations = new Dictionary<int, ViewportManager.ViewportLocations>();

  public Viewport mainViewport
  {
    get;
    private set;
  }

  void Awake()
  {
    // Unity singleton pattern
    if (instance == null)
    {
      instance = this;
    }
    else
    {
      Debug.LogError("Multiple GameManager instances created");
      Destroy(gameObject);
    }

    cInput.Init();
    cInput.SetKey("Primary1", Keys.Xbox1A);
    cInput.SetKey("Secondary1", Keys.Xbox1B);
    cInput.SetKey("Y1", Keys.Xbox1Y);
    cInput.SetKey("Left1", Keys.Xbox1LStickLeft);
    cInput.SetKey("Right1", Keys.Xbox1LStickRight);
    cInput.SetAxis("HorisontalMovement1", "Left1", "Right1", 100.0f, 100.0f);
    cInput.SetKey("Up1", Keys.Xbox1LStickUp);
    cInput.SetKey("Down1", Keys.Xbox1LStickDown);
    cInput.SetAxis("VerticalMovement1", "Down1", "Up1", 100.0f, 100.0f);
    cInput.SetKey("BoardShip1", Keys.Xbox1Y);
    cInput.SetKey("CycleLeft1", Keys.Xbox1BumperLeft);
    cInput.SetKey("CycleRight1", Keys.Xbox1BumperRight);

    cInput.SetKey("KBPrimary1", Keys.E);
    cInput.SetKey("KBSecondary1", Keys.F);
    cInput.SetKey("KBLeft1", Keys.A);
    cInput.SetKey("KBUp1", Keys.W);
    cInput.SetKey("KBRight1", Keys.D);
    cInput.SetKey("KBDown1", Keys.S);

    cInput.SetKey("ZoomIn1", Keys.Xbox1TriggerLeft);
    cInput.SetKey("ZoomOut1", Keys.Xbox1TriggerRight);

    cInput.SetKey("Primary2", Keys.Xbox2A);
    cInput.SetKey("Secondary2", Keys.Xbox2B);
    cInput.SetKey("Left2", Keys.Xbox2LStickLeft);
    cInput.SetKey("Right2", Keys.Xbox2LStickRight);
    cInput.SetAxis("HorisontalMovement2", "Left2", "Right2", 100.0f, 100.0f);
    cInput.SetKey("Up2", Keys.Xbox2LStickUp);
    cInput.SetKey("Down2", Keys.Xbox2LStickDown);
    cInput.SetAxis("VerticalMovement2", "Down2", "Up2", 100.0f, 100.0f);
    cInput.SetKey("BoardShip2", Keys.Xbox2Y);
  }

  // Use this for initialization
  void Start()
  {
    mainViewport = viewportManager.OpenViewport(Viewport.ViewportTypes.Space, ViewportManager.ViewportLocations.Centre);
    mainViewport.FocusTransform(EntityManager.instance.playerShip.transform);
  }

  // Update is called once per frame
  void Update()
  {

  }
}
