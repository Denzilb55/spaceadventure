﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour
{

  public float minAngle;
  public float maxAngle;

  private Vector2 centreAxis;
  private float startAngle;

  public GameObject projectileTemplate;
  public GameObject gunTip;

  void Awake()
  {
    centreAxis = transform.up;
    startAngle = Mathf.Atan2(-centreAxis.x, centreAxis.y) * Mathf.Rad2Deg;
  }

  int layerMask;

  // Use this for initialization
  void Start()
  {
    layerMask = 1 << (int)Layer.Default;

    for (int i = (int)Layer.Team0; i <= (int)Layer.Team3; i++)
    {
      if (i != gameObject.layer)
      {
        layerMask |= i;
      }
    }
  }

  // Update is called once per frame
  void Update()
  {

  }

  private static float GetAngle(Vector2 v1, Vector2 v2)
  {
    var sign = Mathf.Sign(v1.x * v2.y - v1.y * v2.x);
    return Vector2.Angle(v1, v2) * sign;
  }

  public bool isCooldown
  {
    get;
    private set;
  }

  void Cooldown()
  {
    isCooldown = false;
  }

  public void Fire(GameObject target, Vector2 offset)
  {
    if (!isCooldown)
    {
      GameObject fake = Instantiate(projectileTemplate);
      fake.transform.position = gunTip.transform.position;
      fake.GetComponent<Rigidbody2D>().velocity = transform.up * 20;
      Destroy(fake.GetComponent<Projectile>());
      Destroy(fake, 5);

      GameObject projectile = Instantiate(projectileTemplate);
      projectile.transform.position = (Vector2)(target.transform.position + (gunTip.transform.position - transform.parent.position)) + new Vector2(20, 100);

      Vector2 randomAimError = UnityEngine.Random.insideUnitCircle * 2.5f;

      projectile.GetComponent<Projectile>().FireAt(target, offset + randomAimError);

      AudioSource audio = GameManager.instance.GetComponent<AudioSource>();
      audio.PlayOneShot(audio.clip, 0.4f);

      isCooldown = true;
      Invoke("Cooldown", 1);
    }
  }

  public void Aim(float x, float y)
  {
    Vector2 aimAxis = new Vector2(x, y);

    if (aimAxis.sqrMagnitude < 0.5f)
    {
      return;
    }

    aimAxis.Normalize();

    float angle = GetAngle(centreAxis, aimAxis);

    angle = Mathf.Clamp(angle, minAngle, maxAngle);

    transform.localRotation = Quaternion.AngleAxis(angle + startAngle, Vector3.forward);
  }
}
